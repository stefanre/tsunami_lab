###########
Tsunami Lab
###########

This is the Tsunami Lab repository by Philipp and Stefan.

User guide
----------

Install requirements:
_____________________
  .. code-block:: shell

         pip install -r requirements.txt

Run the application:
____________________

    First build the project:

      .. code-block:: shell

         git submodule init
         git submodule update
         scons
    
    Run unit tests:

      .. code-block:: shell

         ./build/tests

    You have to create a directory inputData were the following files are inside:
      - artificialtsunami_bathymetry_1000.nc
      - artificialtsunami_displ_1000.nc
      - chile_gebco20_usgs_250m_bath_fixed.nc
      - chile_gebco20_usgs_250m_displ_fixed.nc
      - tohoku_gebco08_ucsb3_250m_bath.nc
      - tohoku_gebco08_ucsb3_250m_displ.nc

    Run the code with the options:
    Run code in the following way:

        .. code-block:: shell

          ./build/tsunami_lab

    To edit any settings, open the configuration.xml file. You should find a line similar to the following:

        .. code-block:: shell

           <settings   
                cellAmountX="2700" cellAmountY="1500" domainSize="10000.0" solver="2" setup="12" 
                boundaryCondition="1"  stationsFrequency="50.0" offsetX="-200000" offsetY="-750000" domainSizeX="2700000" 
                domainSizeY="1500000" dataCoarseFactor = "10"/>
          <stations>

    Edit the numbers right of the equation symbols in your favour, those are your options:

    - cellsize: number of cells you want 

    - solver: decide whether you want to use
        * Roe solver = 1
        * F-wave solver = 2

    - setup: decide whether you want to see
        * Dam-Break problem = 1
        * Shock-Shock-Wave problem = 2
        * Rare-Rare-Wave problem = 3
        * Bathymetry Example = 4
        * Subcritical Flow = 5
        * Supercritical Flow = 6
        * 1D Tsunami example = 7
        * 2D circular Dam-Break = 8
        * 2D artificial tsunami = 9
        * 2D artificial tsunami out of a netcdf file = 10
        * Chile Event = 11 
        * Tsuanmi Event = 12 

    - boundaryCondition: sets whether boundary cells reflect momentums, similar to a wall, or if water simply "flows out" there
        * Left side  | Right side
        * outflow    | outflow    = 1
        * outflow    | reflection = 2
        * reflection | outflow    = 3
        * reflection | reflection = 4

    Furthermore, you should find a block of lines looking similar to:

        .. code-block:: shell

          <Station Name="001" X="1" Y="1"/>

    They offer creating specific meassuring stations, solid throughout a simulation, creating their own .csv output file. The name option sets how those output files are named, X and Y are the coordinates of the stations
