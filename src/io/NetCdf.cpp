/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * IO-routines for writing data to netCdf file.
 **/
 
#include "NetCdf.h"


void tsunami_lab::io::NetCdf::checkNcErr( int i_err ){
  if( i_err ) {
    std::cerr << "Error: "
              << nc_strerror( i_err )
              << std::endl;
  }
}

tsunami_lab::io::NetCdf::NetCdf( t_idx i_nXCells,
                                 t_idx i_nYCells,
                                 const char * i_fileName,
                                 t_idx i_decide,
                                 t_idx i_k ){
  std::cout << "generating netcdf-file: " << i_fileName << std::endl;
  m_nXCells = i_nXCells;
  m_nYCells = i_nYCells;
  m_k = i_k;
  m_nXCellsCoarsed = m_nXCells / m_k;
  m_nYCellsCoarsed = m_nYCells / m_k;
  
  // set up data
  int l_dimHId[3];
  int l_dimBId[2];
  int l_err;

  // set up netCDF-file
  l_err = nc_create( i_fileName,
                      NC_CLOBBER | NC_64BIT_OFFSET,   
                      &m_ncId );   
  checkNcErr( l_err );

  // define dimensions and variables
  l_err = nc_def_dim( m_ncId,     
                      "x",     
                      m_nXCellsCoarsed,
                      &m_dimXId );
  checkNcErr( l_err );
  l_err = nc_def_var(m_ncId, "x", NC_FLOAT, 1, &m_dimXId, &m_varXId);
  checkNcErr(l_err);
  l_err = nc_put_att_text(m_ncId, m_varXId, "units", 6, "meters");
  checkNcErr(l_err);
  l_err = nc_put_att_text(m_ncId, m_varXId, "axis", 1, "X");
  checkNcErr(l_err);

  l_err = nc_def_dim( m_ncId,
                      "y",       
                      m_nYCellsCoarsed,  
                      &m_dimYId );
  checkNcErr( l_err );
  l_err = nc_def_var(m_ncId, "y", NC_FLOAT, 1, &m_dimYId, &m_varYId);
  checkNcErr(l_err);
  l_err = nc_put_att_text(m_ncId, m_varYId, "units", 6, "meters");
  checkNcErr(l_err);
  l_err = nc_put_att_text(m_ncId, m_varYId, "axis", 1, "Y");
  checkNcErr(l_err);
  
  l_err = nc_def_dim( m_ncId, 
                      "time",   
                      NC_UNLIMITED,
                      &m_dimTimeId ); 
  checkNcErr( l_err );
  l_err = nc_def_var(m_ncId, "time", NC_FLOAT, 1, &m_dimTimeId, &m_varTimeId);
  checkNcErr(l_err);
  l_err = nc_put_att_text(m_ncId, m_varTimeId, "units", 7, "seconds"); 
  checkNcErr(l_err);
 

  l_dimHId[0] = m_dimTimeId;
  l_dimHId[1] = m_dimYId;
  l_dimHId[2] = m_dimXId;
  l_dimBId[0] = m_dimYId;
  l_dimBId[1] = m_dimXId;

  // set bathymetry
  l_err = nc_def_var( m_ncId,     
                      "b",     
                      NC_FLOAT,     
                      2,          
                      l_dimBId,
                      &m_varBathyId );
  checkNcErr( l_err );
  l_err = nc_put_att_text(m_ncId, m_varBathyId, "units", 6, "meters"); 
  checkNcErr( l_err );

  // set height variable
  if( i_decide){
    l_err = nc_def_var( m_ncId,  
                        "h",   
                        NC_FLOAT,     
                        3,          
                        l_dimHId,   
                        &m_varHeightId ); 
    checkNcErr( l_err );
    l_err = nc_put_att_text(m_ncId, m_varHeightId, "units", 6, "meters"); 
    checkNcErr( l_err );
  } else {
    l_err = nc_def_var( m_ncId,  
                        "h",   
                        NC_FLOAT,     
                        2,          
                        l_dimBId,   
                        &m_varHeightId ); 
    checkNcErr( l_err );
    l_err = nc_put_att_text(m_ncId, m_varHeightId, "units", 6, "meters"); 
    checkNcErr( l_err );
  }
  // set momentum x variable
  if( i_decide ){
    l_err = nc_def_var( m_ncId,  
                        "hu",   
                        NC_FLOAT,     
                        3,          
                        l_dimHId,   
                        &m_varMomXId ); 
    checkNcErr( l_err );
    l_err = nc_put_att_text(m_ncId, m_varMomXId, "units", 8, "momentum"); 
    checkNcErr( l_err );
  } else {
    l_err = nc_def_var( m_ncId,  
                        "hu",   
                        NC_FLOAT,     
                        2,          
                        l_dimBId,   
                        &m_varMomXId ); 
    checkNcErr( l_err );
    l_err = nc_put_att_text(m_ncId, m_varMomXId, "units", 8, "momentum"); 
    checkNcErr( l_err );
  }
  // set momentum y variable
  if( i_decide ){
    l_err = nc_def_var( m_ncId,  
                        "hv",   
                        NC_FLOAT,     
                        3,          
                        l_dimHId,   
                        &m_varMomYId ); 
    checkNcErr( l_err );
    l_err = nc_put_att_text(m_ncId, m_varMomYId, "units", 8, "momentum"); 
    checkNcErr( l_err );
  } else {
    l_err = nc_def_var( m_ncId,  
                        "hv",   
                        NC_FLOAT,     
                        2,          
                        l_dimBId,   
                        &m_varMomYId ); 
    checkNcErr( l_err );
    l_err = nc_put_att_text(m_ncId, m_varMomYId, "units", 8, "momentum"); 
    checkNcErr( l_err );
  }
  
  // End define mode.
  l_err = nc_enddef( m_ncId ); 
  checkNcErr( l_err );
}
tsunami_lab::io::NetCdf::~NetCdf(){
  delete[] m_timeStep;
}

void tsunami_lab::io::NetCdf::initialize( t_real         i_dx,
                                          t_real         i_dy,
                                          t_real         i_stride,
                                          t_real const * i_b,
                                          t_real         i_offsetX,
                                          t_real         i_offsetY,
                                          const char * i_fileName ){
  int l_err, l_ncId;
  l_err = nc_open(i_fileName, NC_WRITE, &l_ncId);

  i_dx *= m_k;
  i_dy *= m_k;

  t_real * l_Xpos = new t_real[m_nXCellsCoarsed];
  for( t_idx i_c = 0; i_c < m_nXCellsCoarsed; i_c++){
    l_Xpos[i_c] = (i_c + 0.5) * i_dx + i_offsetX;
  }
  t_real * l_Ypos = new t_real[m_nYCellsCoarsed];
  for( t_idx i_c = 0; i_c < m_nYCellsCoarsed; i_c++){
    l_Ypos[i_c] = (i_c + 0.5) * i_dy + i_offsetY;
  }
  // put x and y 
  l_err = nc_put_var_float( l_ncId, m_varXId, l_Xpos );
  checkNcErr( l_err );
  l_err = nc_put_var_float( l_ncId, m_varYId, l_Ypos );
  checkNcErr( l_err );

  delete[] l_Xpos;
  delete[] l_Ypos;

  t_real * l_bath = new t_real[( m_nYCellsCoarsed * m_nXCellsCoarsed )];

   for( t_idx l_iy = 0; l_iy < m_nYCellsCoarsed; l_iy ++ ) {
    for( t_idx l_ix = 0; l_ix < m_nXCellsCoarsed; l_ix ++ ) {
      // derive average of k² cells, write into one (coarsing)
      t_real l_sum = 0;
      t_idx l_num = 0;
      for( t_idx l_iyK = l_iy * m_k; l_iyK < (l_iy + 1) * m_k && l_iyK < m_nYCells; l_iyK ++ ) {
        for( t_idx l_ixK = l_ix * m_k; l_ixK < (l_ix + 1) * m_k && l_ixK < m_nXCells; l_ixK ++ ) {
          l_sum += i_b [ (l_iyK + 1) * (int)i_stride + (l_ixK + 1) ];
          l_num ++;
        }
      }
      // write average into output bathemetry
      l_bath[ l_iy * m_nXCellsCoarsed + l_ix ] = l_sum / l_num;
    }
   }

  // put bathymetry
  l_err = nc_put_var_float( l_ncId, m_varBathyId, l_bath );
  checkNcErr( l_err );

  delete[] l_bath;

  l_err = nc_close( l_ncId );
  checkNcErr( l_err );
}

void tsunami_lab::io::NetCdf::write( t_real const * i_h,
                                     t_real const * i_hu,
                                     t_real const * i_hv,
                                     t_idx          i_stride,
                                     t_idx          i_timeStepCount,
                                     t_real         i_time,
                                     const char *   i_fileName,
                                     t_idx          i_decide,
                                     t_idx          i_checkpointCount ){
  int l_err, l_ncId;
  l_err = nc_open(i_fileName, NC_WRITE, &l_ncId);
  size_t l_start[3], l_count[3]; 

  l_count[0] = 1;
  l_count[1] = m_nYCellsCoarsed;
  l_count[2] = m_nXCellsCoarsed;
  l_start[1] = 0;
  l_start[2] = 0;

  t_real * l_height = new t_real[m_nYCellsCoarsed * m_nXCellsCoarsed];
  t_real * l_hu = new t_real[m_nYCellsCoarsed * m_nXCellsCoarsed];
  t_real * l_hv = new t_real[m_nYCellsCoarsed * m_nXCellsCoarsed];

  for( t_idx l_iy = 0; l_iy < m_nYCellsCoarsed; l_iy++ ) {
    for( t_idx l_ix = 0; l_ix < m_nXCellsCoarsed; l_ix++ ) {
      // derive average of k² cells, write into one (coarsing)
      t_real l_sumHeight = 0;
      t_real l_sumMomentumX = 0;
      t_real l_sumMomentumY = 0;
      t_idx l_num = 0;
      for( t_idx l_iyK = l_iy * m_k; l_iyK < (l_iy + 1) * m_k && l_iyK < m_nYCells; l_iyK ++ ) {
        for( t_idx l_ixK = l_ix * m_k; l_ixK < (l_ix + 1) * m_k && l_ixK < m_nXCells; l_ixK ++ ) {
          l_sumHeight     += i_h [ (l_iyK + 1) * (int)i_stride + (l_ixK + 1)];
          l_sumMomentumX  += i_hu[ (l_iyK + 1) * (int)i_stride + (l_ixK + 1)];
          l_sumMomentumY  += i_hv[ (l_iyK + 1) * (int)i_stride + (l_ixK + 1)];
          l_num ++;
        }
      }
      // write averages
      l_height  [ l_iy * m_nXCellsCoarsed + l_ix ] = l_sumHeight / l_num;
      l_hu      [ l_iy * m_nXCellsCoarsed + l_ix ] = l_sumMomentumX / l_num;
      l_hv      [ l_iy * m_nXCellsCoarsed + l_ix ] = l_sumMomentumY / l_num;
    }
  }
  
  l_start[0] = i_timeStepCount;
  if( i_decide ){
    l_err = nc_put_vara_float(l_ncId, m_varHeightId, l_start, l_count, &l_height[0]);
    checkNcErr( l_err );

    l_err = nc_put_vara_float(l_ncId, m_varMomXId, l_start, l_count, &l_hu[0]);
    checkNcErr( l_err );

    l_err = nc_put_vara_float(l_ncId, m_varMomYId, l_start, l_count, &l_hv[0]);
    checkNcErr( l_err );

    l_err = nc_put_var1_float(l_ncId, m_varTimeId, &i_timeStepCount, &i_time);
    checkNcErr( l_err );
  } else {
    t_idx l_startCheck[2] = {0,0};
    t_idx l_countCheck[2] = { m_nYCellsCoarsed, m_nXCellsCoarsed};
    l_err = nc_put_vara_float(l_ncId, m_varHeightId, l_startCheck, l_countCheck, &l_height[0]);
    checkNcErr( l_err );

    l_err = nc_put_vara_float(l_ncId, m_varMomXId, l_startCheck, l_countCheck, &l_hu[0]);
    checkNcErr( l_err );

    l_err = nc_put_vara_float(l_ncId, m_varMomYId, l_startCheck, l_countCheck, &l_hv[0]);
    checkNcErr( l_err );

    l_err = nc_put_var1_float(l_ncId, m_varTimeId, &i_checkpointCount, &i_time);
    checkNcErr( l_err );
  }

  delete[] l_height;
  delete[] l_hu;
  delete[] l_hv;

  l_err = nc_close( l_ncId);
  checkNcErr( l_err );


}

void tsunami_lab::io::NetCdf::read( const char * i_inputFileName,
                                    const char * i_varName,
                                    t_real *     o_data ){
  int l_err;
  int l_ncId, l_varId;

  l_err = nc_open(i_inputFileName, NC_NOWRITE, &l_ncId);
  checkNcErr(l_err);

  l_err = nc_inq_varid(l_ncId, i_varName, &l_varId);
  checkNcErr(l_err);

  l_err = nc_get_var_float(l_ncId, l_varId, o_data);
  checkNcErr(l_err);

  l_err = nc_close(l_ncId);
  checkNcErr(l_err);

}

size_t tsunami_lab::io::NetCdf::getDimLength( const char * i_inputFileName,
                                              const char * i_varName){
  
  int l_err;
  int l_ncId, l_varId;
  size_t l_length;  

  l_err = nc_open(i_inputFileName, NC_NOWRITE, &l_ncId);
  checkNcErr( l_err );

  l_err = nc_inq_varid(l_ncId, i_varName, &l_varId);
  checkNcErr( l_err );

  l_err = nc_inq_dimlen(l_ncId, l_varId, &l_length);
  checkNcErr( l_err );

  l_err = nc_close(l_ncId);
  checkNcErr( l_err );

  return l_length;
}