/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Stations to track output at specific points
 **/
#include "../constants.h"
#include <string>
#include <fstream>


namespace tsunami_lab {
  namespace io {
    class Stations;
  }
}

class tsunami_lab::io::Stations {
    private:
      
      //! x-coordinate of station
      t_real m_x = 0;

      //! y-coordinate of station
      t_real m_y = 0;

      //! x-cell number that the station is in
      t_idx m_x_cell = 0;

      //! y-cell number that the station is in
      t_idx m_y_cell = 0;

      //! position in the 1 dimensional array
      t_idx m_pos = 0;

      //! frequency
      t_real m_frequency = 0 ; 
      
      //! Output Stream for station data
      std::ofstream m_file;

    public:
      /**
       * Constructor.
       * 
       * @param i_name station name
       * @param i_x x-coordinate of station
       * @param i_y y-coordinate of station
       * @param i_cellSize size of one cell (for now just with l_xn == l_xy)
       * @param i_frequency frequency with which the measurement is made in seconds
       * @param i_mCells number of cells in x and y direction
       **/
      Stations( std::string i_name,
                t_real      i_x, 
                t_real      i_y,
                t_real      i_cellSize,
                t_real      i_frequency,
                t_real      i_mCells );
      /**
       * Adds the data for a specific time section to the station related csv-file, then triggers write operation for next station in list.
       * @param i_time time stamp in seconds
       * @param i_height height at the station at i_time
       * @param i_momentumX momentum in x direction
       * @param i_momentumY momentum in y direction
       * @param io_stream stream to write to file
       **/
      void write( t_real         i_time,
                  t_real const * i_height,
                  t_real const * i_momentumX,
                  t_real const * i_momentumY );

      /**
       * Deconstructor, closes the csv-output file.
      */
      ~Stations();

      t_real getFrequency(){
        return m_frequency;
      }

      t_real getX(){
        return m_x;
      }

      t_real getY(){
        return m_y;
      }
};