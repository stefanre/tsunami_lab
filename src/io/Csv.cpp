/**
 * @author Alexander Breuer (alex.breuer AT uni-jena.de)
 *
 * @section DESCRIPTION
 * IO-routines for writing a snapshot as Comma Separated Values (CSV).
 **/
#include "Csv.h"

void tsunami_lab::io::Csv::write( t_real               i_dxy,
                                  t_idx                i_nx,
                                  t_idx                i_ny,
                                  t_idx                i_stride,
                                  t_real       const * i_h,
                                  t_real       const * i_b,
                                  t_real       const * i_hu,
                                  t_real       const * i_hv,
                                  std::ostream       & io_stream ) {
  // write the CSV header
  io_stream << "x,y";
  if( i_h  != nullptr ) io_stream << ",height";
  if( i_hu != nullptr ) io_stream << ",momentum_x";
  if( i_hv != nullptr ) io_stream << ",momentum_y";
  if( i_b != nullptr ) io_stream << ",bathymetry";
  io_stream << "\n";

  // iterate over all cells
  for( t_idx l_iy = 0; l_iy < i_ny; l_iy++ ) {
    for( t_idx l_ix = 0; l_ix < i_nx; l_ix++ ) {
      // derive coordinates of cell center
      t_real l_posX = (l_ix + 0.5) * i_dxy;
      t_real l_posY = (l_iy + 0.5) * i_dxy;

      t_idx l_id = l_iy * i_stride + l_ix;

      // write data
      io_stream << l_posX << "," << l_posY;
      if( i_h  != nullptr ) io_stream << "," << i_h[l_id] + i_b[l_id];
      if( i_hu != nullptr ) io_stream << "," << i_hu[l_id];
      if( i_hv != nullptr ) io_stream << "," << i_hv[l_id];
      if( i_b != nullptr ) io_stream << "," << i_b[l_id];
      io_stream << "\n";
    }
  }
  io_stream << std::flush;
}


void tsunami_lab::io::Csv::splitStr( std::string i_str,
						                         std::vector<std::string> &o_values){

  std::vector<std::string> l_result;
	std::stringstream l_ss(i_str);
	std::string l_s;
  // split line by commas
	while (std::getline(l_ss, l_s, ',')){
			l_result.push_back(l_s);
	}
	
	o_values = l_result;
}

void tsunami_lab::io::Csv::readTsunami1d( t_real * o_bathymetry ){
  
  // initialize file
  std::ifstream l_file;
  // open file
  l_file.open("tsunami1d_data.csv");
  if(!l_file.is_open()){
    std::cerr << "*******************" << std::endl;
    std::cerr << "problem with file!!" << std::endl;
    std::cerr << "*******************" << std::endl;
  }

  // get bathymetry values from csv file 
  std::string l_line;
  std::vector<std::string> l_h ;
  int counter = 0;

  while (std::getline(l_file, l_line)){
    // skip first row
    if(l_line.substr(0, 1) == "x") continue;

    // split the .csv line
    splitStr(l_line, l_h);

    // get bathymetry 
    o_bathymetry[counter] = std::stod(l_h[3]);
    counter++;
    

  }
  l_file.close();

}