/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * IO-routines for writing data to netCdf file.
 **/
#ifndef TSUNAMI_LAB_IO_NETCDF
#define TSUNAMI_LAB_IO_NETCDF

#include "../constants.h"
#include <netcdf.h>
#include <iostream>

namespace tsunami_lab {
  namespace io {
    class NetCdf;
  }
}

class tsunami_lab::io::NetCdf {

  private:

    //! set up data 
    int m_ncId, m_dimXId, m_dimYId, m_dimTimeId, m_varHeightId, m_varXId, m_varYId, m_varTimeId, m_varBathyId, m_varMomXId, m_varMomYId;

    //! number of cells in x direction
    t_idx m_nXCells = 0;

    //! number of cells in y direction
    t_idx m_nYCells = 0;

    //! array of time steps
    t_real * m_timeStep;

    t_idx m_nTimeSteps = 0;

    //! data coarse dividend
    t_idx m_k = 1;

    //! number of output x components after coarsing
    t_idx m_nXCellsCoarsed = 0;

    //! number of output y components after coarsing
    t_idx m_nYCellsCoarsed = 0;



    /**
      * Checks if there is an error
      *
      * @param i_err error value
      **/
    static void checkNcErr( int i_err);

  public:
    
    /**
      * Constructor
      *
      * @param i_nXCells number of cells in x direction
      * @param i_nYCells number of cells in y direction
      * @param i_fileName name of the created file
      * @param i_decide decide if 1 output file if 0 checkpoint file
      * @param i_k Coarse Output dividend
      **/
    NetCdf( t_idx        i_nXCells,
            t_idx        i_nYCells,
            const char * i_fileName,
            t_idx        i_decide,
            t_idx        i_k );

    /**
      * Destructor
      **/
    ~NetCdf();
    /**
      * Initialize x, y and bathymetry
      *
      * @param i_dx cell width in x direction.
      * @param i_dy cell width in y direction.
      * @param i_stride stride in y direction.
      * @param i_b values of the bathymetry.
      * @param i_fileName name of the file into which the data is written
      **/
    void initialize( t_real         i_dx,
                     t_real         i_dy,
                     t_real         i_stride,
                     t_real const * i_b,
                     t_real         i_offsetX,
                     t_real         i_offsetY,
                     const char *   i_fileName  );
    /**
     * Writes the data as netcdf data to output.nc.
     *
     * @param i_h water height of the cells; optional: use nullptr if not required.
     * @param i_hu momentum in x direction.
     * @param i_hv momentum in y direction.
     * @param i_stride stride in y-direction.
     * @param i_timeStepCount count of the timestep
     * @param i_time current simulation time.
     * @param i_fileName name of the file into which the data is written
     * @param i_decide if 1 output file if 0 checkpoint file
     * @param i_checkpointCount counts the number of checkpoints made
     **/
    void write( t_real const * i_h,
                t_real const * i_hu,
                t_real const * i_hv,
                t_idx          i_stride,
                t_idx          i_timeStepCount,
                t_real         i_time,
                const char *   i_fileName,
                t_idx          i_decide ,
                t_idx          i_checkpointCount );

    /**
      * Reads data from a netcdf file.
      *
      * @param i_inputFileName name of the netcdf file .
      * @param i_varName name of the variable to be read.
      * @param o_data array into which the data is written.
      */
    static void read( const char *   i_inputFileName,
                      const char *   i_varName,
                      t_real *       o_data );


    /**
      * Gets the length of a dimension
      *
      * @param i_inputFileName name of the netcdf file .
      * @param i_varName name of the variable to be read.
      **/
    static size_t getDimLength( const char * i_inputFileName,
                                const char * i_varName);
};

#endif