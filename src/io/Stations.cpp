/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Stations to track output at specific points
 **/
 #include "Stations.h"
 #include <iostream>
 #include <fstream>

 tsunami_lab::io::Stations::Stations(  std::string    i_name,
                                       t_real         i_x, 
                                       t_real         i_y,
                                       t_real         i_cellSize,
                                       t_real         i_frequency,
                                       t_real         i_mCells ){
  
  // set values
  m_x = i_x;
  m_y = i_y;
  m_x_cell = (t_idx)(i_x / i_cellSize);
  m_y_cell = (t_idx)(i_y / i_cellSize);
  m_pos = ((i_mCells+2) * (m_x_cell+1)) + m_y_cell+1;

  m_frequency = i_frequency;

  std::ofstream m_file;
  m_file.open("station_" + i_name + ".csv");

  // write CSV header
  m_file << "Station " << i_name << " at " << i_x << ", " << i_y << "\n";
  m_file << "time,height,momentum_x,momentum_y\n";
}

void tsunami_lab::io::Stations::write(  t_real         i_time,
                                        t_real const * i_height,
                                        t_real const * i_momentumX,
                                        t_real const * i_momentumY ){
  if(i_height != nullptr) m_file << i_time << "," << i_height[m_pos];
  if(i_momentumX != nullptr ) m_file << "," << i_momentumX[m_pos];
  if(i_momentumY != nullptr ) m_file << "," << i_momentumY[m_pos];
  m_file << "\n";
}

tsunami_lab::io::Stations::~Stations() {
  m_file.close();
}