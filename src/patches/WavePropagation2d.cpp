/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Two-dimensional wave propagation patch.
 **/
#include "WavePropagation2d.h"
#include "../solvers/Roe.h"
#include "../solvers/Fwave.h"
#include <iostream>


tsunami_lab::patches::WavePropagation2d::WavePropagation2d( t_idx i_nXCells, 
                                                            t_idx i_nYCells ) {
  m_nXCells = i_nXCells;
  m_nYCells = i_nYCells;
  m_nCells = (i_nXCells+2) * (i_nYCells+2);

  // allocate memory including a single ghost cell on each side
  for( unsigned short l_st = 0; l_st < 2; l_st++ ) {
    m_h[l_st] = new t_real[ m_nCells ];
    m_hu[l_st] = new t_real[ m_nCells ];
    m_hv[l_st] = new t_real[ m_nCells ];
  }
  m_b = new t_real[ m_nCells ];

  // init to zero
  #pragma omp parallel for
  for( unsigned short l_st = 0; l_st < 2; l_st++ ) {
    for( t_idx l_ce = 0; l_ce < m_nCells; l_ce++ ) {
      m_h[l_st][l_ce] = 0;
      m_hu[l_st][l_ce] = 0;
    }
  }
  for( t_idx l_ce = 0; l_ce < m_nCells; l_ce++){
    m_b[l_ce] = 0;
  }

}

tsunami_lab::patches::WavePropagation2d::~WavePropagation2d() {
  for( unsigned short l_st = 0; l_st < 2; l_st++ ) {
    delete[] m_h[l_st];
    delete[] m_hu[l_st];
    delete[] m_hv[l_st];
  }
  delete[] m_b;
}

void tsunami_lab::patches::WavePropagation2d::timeStep( t_real i_scaling,
                                                        unsigned short i_solv ) {
  // pointers to old and new data
  t_real * l_hOld = m_h[m_step];
  t_real * l_huOld = m_hu[m_step];
  t_real * l_hvOld = m_hv[m_step];

  m_step = (m_step+1) % 2;
  t_real * l_hNew =  m_h[m_step];
  t_real * l_huNew = m_hu[m_step];
  t_real * l_hvNew = m_hv[m_step];
  

  // init new cell quantities
  #pragma omp parallel for
  for( t_idx l_ce = 1; l_ce < m_nCells-1; l_ce++ ) {
    l_hNew[l_ce] = l_hOld[l_ce];
    l_huNew[l_ce] = l_huOld[l_ce];
    l_hvNew[l_ce] = l_hvOld[l_ce];
  }

  // doing the x sweep!
  #pragma omp parallel for
  for( t_idx l_ey = 0; l_ey < m_nYCells+2; l_ey++){
    // iterate over edges and update with Riemann solutions
    for( t_idx l_ex = 0; l_ex < m_nXCells+1; l_ex++ ) {
      // determine left and right cell-id
      t_idx l_ceL = l_ey*(m_nXCells+2) + l_ex;
      t_idx l_ceR = l_ey*(m_nXCells+2) + l_ex + 1;

      // do nothing if both cells are dry
      if( l_hOld[l_ceL] < 0.001 && l_hOld[l_ceR] < 0.001) {
        continue;
      }
      if( l_hOld[l_ceL] < 0.001){
        l_hNew[l_ceR] = l_hOld[l_ceR];
        l_huNew[l_ceR] = - l_huOld[l_ceR];
        l_huOld[l_ceR] = -l_huOld[l_ceR];
        continue;
      }
      if( l_hOld[l_ceR] < 0.001){
        l_hNew[l_ceL] = l_hOld[l_ceL];
        l_huNew[l_ceL] = - l_huOld[l_ceL];
        l_huOld[l_ceL] = -l_huOld[l_ceL];        
        continue;
      }
  
      // compute net-updates
      t_real l_netUpdates[2][2];

      if( i_solv == 1 ) {
        solvers::Roe::netUpdates( l_hOld[l_ceL],
                                  l_hOld[l_ceR],
                                  l_huOld[l_ceL],
                                  l_huOld[l_ceR],
                                  l_netUpdates[0],
                                  l_netUpdates[1] );
      } else {
        solvers::Fwave::netUpdatesWbathymetry( l_hOld[l_ceL],
                                              l_hOld[l_ceR],
                                              l_huOld[l_ceL],
                                              l_huOld[l_ceR],
                                              m_b[l_ceL],
                                              m_b[l_ceR],
                                              l_netUpdates[0],
                                              l_netUpdates[1] );
      }
      // update the cells' quantities
      l_hNew[l_ceL]  -= i_scaling * l_netUpdates[0][0];
      l_huNew[l_ceL] -= i_scaling * l_netUpdates[0][1];

      l_hNew[l_ceR]  -= i_scaling * l_netUpdates[1][0];
      l_huNew[l_ceR] -= i_scaling * l_netUpdates[1][1];
    }
  }
  // init new cell quantities
  #pragma omp parallel for
  for( t_idx l_ce = 1; l_ce < m_nCells-1; l_ce++ ) {
    l_hOld[l_ce] = l_hNew[l_ce];
  }
  // doing the y sweep!
  #pragma omp parallel for
  for( t_idx l_ex = 0; l_ex < m_nXCells+2; l_ex++){
    
    for( t_idx l_ey = 0; l_ey < m_nYCells+1; l_ey++ ) {
       
      // determine left and right cell-id
      t_idx l_ceL = l_ey*(m_nXCells+2) + l_ex ;
      t_idx l_ceR = (l_ey+1)*(m_nXCells+2) + l_ex ;

      // do nothing if both cells are dry
      if( l_hOld[l_ceL] < 0.001 && l_hOld[l_ceR] < 0.001) {
        continue;
      }
      if( l_hOld[l_ceL] < 0.001){
        l_hNew[l_ceR] = l_hOld[l_ceR];
        l_hvNew[l_ceR] = - l_hvOld[l_ceR];
        l_hvOld[l_ceR] = -l_hvOld[l_ceR];
        continue;
      }
      if( l_hOld[l_ceR] < 0.001){
        l_hNew[l_ceL] = l_hOld[l_ceL];
        l_hvNew[l_ceL] = - l_hvOld[l_ceL];
        l_hvOld[l_ceL] = -l_hvOld[l_ceL];        
        continue;
      }

      // do nothing if both cells are dry
      if( l_hOld[l_ceL] < 0.001 && l_hOld[l_ceR] < 0.001) {
        continue;
      }
      if( l_hOld[l_ceL] < 0.001){
        l_hNew[l_ceR] = l_hOld[l_ceR];
        l_hvNew[l_ceR] = - l_hvOld[l_ceR];
        l_hvOld[l_ceR] = -l_hvOld[l_ceR];
        continue;
      }
      if( l_hOld[l_ceR] < 0.001){
        l_hNew[l_ceL] = l_hOld[l_ceL];
        l_hvNew[l_ceL] = - l_hvOld[l_ceL];
        l_hvOld[l_ceL] = -l_hvOld[l_ceL];        
        continue;
      }

      // compute net-updates
      t_real l_netUpdates[2][2];

      if( i_solv == 1 ) {
        solvers::Roe::netUpdates( l_hOld[l_ceL],
                                  l_hOld[l_ceR],
                                  l_hvOld[l_ceL],
                                  l_hvOld[l_ceR],
                                  l_netUpdates[0],
                                  l_netUpdates[1] );
      } else {
        solvers::Fwave::netUpdatesWbathymetry( l_hOld[l_ceL],
                                               l_hOld[l_ceR],
                                               l_hvOld[l_ceL],
                                               l_hvOld[l_ceR],
                                               m_b[l_ceL],
                                               m_b[l_ceR],
                                               l_netUpdates[0],
                                               l_netUpdates[1] );
      }

      // update the cells' quantities
      l_hNew[l_ceL] -= i_scaling * l_netUpdates[0][0];
      l_hvNew[l_ceL] -= i_scaling * l_netUpdates[0][1];

      l_hNew[l_ceR]  -= i_scaling * l_netUpdates[1][0];
      l_hvNew[l_ceR] -= i_scaling * l_netUpdates[1][1];
    }
  }
}

void tsunami_lab::patches::WavePropagation2d::setGhostOutflow() {

  t_real * l_h = m_h[m_step];
  t_real * l_hu = m_hu[m_step];
  t_real * l_hv = m_hv[m_step];
  t_real * l_b = m_b;

  // set bottom bounds
  #pragma omp parallel for
  for(t_idx l_a = 1 ; l_a < m_nXCells +1; l_a++){
    l_h[l_a] = l_h[l_a+ m_nXCells+2];
    l_hu[l_a] = l_hu[l_a+ m_nXCells+2];
    l_hv[l_a] = l_hv[l_a+ m_nXCells+2];
    l_b[l_a] = l_b[l_a+ m_nXCells+2];
  }
  // set left bounds 
  #pragma omp parallel for
  for(t_idx l_a = 1 ; l_a < m_nYCells + 1; l_a++){
    l_h[l_a * (m_nXCells+2)] = l_h[l_a * (m_nXCells+2)+1];
    l_hu[l_a * (m_nXCells+2)] = l_hu[l_a * (m_nXCells+2)+1];
    l_hv[l_a * (m_nXCells+2)] = l_hv[l_a * (m_nXCells+2)+1];
    l_b[l_a * (m_nXCells+2)] = l_b[l_a * (m_nXCells+2)+1];
  }
  // set top bounds
  #pragma omp parallel for
  for(t_idx l_a = 1 ; l_a < m_nXCells + 1; l_a++){
    l_h[ l_a + (m_nXCells+2)*(m_nYCells+1) ] = l_h[ l_a + (m_nXCells+2)*(m_nYCells) ];
    l_hu[ l_a + (m_nXCells+2)*(m_nYCells+1) ] = l_hu[ l_a + (m_nXCells+2)*(m_nYCells) ];
    l_hv[ l_a + (m_nXCells+2)*(m_nYCells+1) ] = l_hv[ l_a + (m_nXCells+2)*(m_nYCells) ];
    l_b[ l_a + (m_nXCells+2)*(m_nYCells+1) ] = l_b[ l_a + (m_nXCells+2)*(m_nYCells) ];
  }
  // set right bounds
  #pragma omp parallel for
  for(t_idx l_a = 1 ; l_a < m_nYCells + 1; l_a++){
    l_h[ l_a * (m_nXCells+2)+ (m_nXCells+1) ] = l_h[ l_a * (m_nXCells+2)+ (m_nXCells) ];
    l_hu[ l_a * (m_nXCells+2)+ (m_nXCells+1) ] = l_hu[ l_a * (m_nXCells+2)+ (m_nXCells) ];
    l_hv[ l_a * (m_nXCells+2)+ (m_nXCells+1) ] = l_hv[ l_a * (m_nXCells+2)+ (m_nXCells) ];
    l_b[ l_a * (m_nXCells+2)+ (m_nXCells+1) ] = l_b[ l_a * (m_nXCells+2)+ (m_nXCells) ];
  }
  // set left bottom corner
  l_h[0] = l_h[m_nXCells+3];
  l_hu[0] = l_hu[m_nXCells+3];
  l_hv[0] = l_hv[m_nXCells+3];
  l_b[0] = l_b[m_nXCells+3];

  // set left top corner
  l_h[(m_nYCells+1)*(m_nXCells+2)] = l_h[(m_nYCells)*(m_nXCells+2)+1];
  l_hu[(m_nYCells+1)*(m_nXCells+2)] = l_hu[(m_nYCells)*(m_nXCells+2)+1];
  l_hv[(m_nYCells+1)*(m_nXCells+2)] = l_hv[(m_nYCells)*(m_nXCells+2)+1];
  l_b[(m_nYCells+1)*(m_nXCells+2)] = l_b[(m_nYCells)*(m_nXCells+2)+1];

  // set right bottom corner
  l_h[(m_nXCells+1)] = l_h[(m_nXCells+2)+m_nXCells];
  l_hu[(m_nXCells+1)] = l_hu[(m_nXCells+2)+m_nXCells];
  l_hv[(m_nXCells+1)] = l_hv[(m_nXCells+2)+m_nXCells];
  l_b[(m_nXCells+1)] = l_b[(m_nXCells+2)+m_nXCells];

  // set right top corner 
  l_h[(m_nYCells+2)*(m_nXCells+2)-1] = l_h[(m_nXCells+2)*(m_nYCells+1)-2];
  l_hu[(m_nYCells+2)*(m_nXCells+2)-1] = l_hu[(m_nXCells+2)*(m_nYCells+1)-2];
  l_hv[(m_nYCells+2)*(m_nXCells+2)-1] = l_hv[(m_nXCells+2)*(m_nYCells+1)-2];
  l_b[(m_nYCells+2)*(m_nXCells+2)-1] = l_b[(m_nXCells+2)*(m_nYCells+1)-2];
}

// TODO get around these implementations
void tsunami_lab::patches::WavePropagation2d::setGhostReflecting(){}

void tsunami_lab::patches::WavePropagation2d::setGhostOutflowReflecting(){}

void tsunami_lab::patches::WavePropagation2d::setGhostReflectingOutflow(){}
