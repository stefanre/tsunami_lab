/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Two-dimensional wave propagation patch.
 **/
#ifndef TSUNAMI_LAB_PATCHES_WAVE_PROPAGATION_2D
#define TSUNAMI_LAB_PATCHES_WAVE_PROPAGATION_2D

#include "WavePropagation.h"

namespace tsunami_lab {
  namespace patches {
    class WavePropagation2d;
  }
}

class tsunami_lab::patches::WavePropagation2d: public WavePropagation {
  private:

    //! current step which indicates the active values in the arrays below
    unsigned short m_step = 0;

    //! number of cells discretizing the computational domain
    t_idx m_nXCells = 0;
    t_idx m_nYCells = 0;
    t_idx m_nCells = 0;

    //! water heights for the current and next time step for all cells
    t_real * m_h[2] = { nullptr, nullptr };

    //! momenta for the current and next time step for all cells in x direction
    t_real * m_hu[2] = { nullptr, nullptr };

    //! momenta for the current and next time step for all cells in y direction
    t_real * m_hv[2] = { nullptr, nullptr };

    //! bathymetry for the current and next time step for all cells
    t_real * m_b = nullptr;
  
  public: 

    /**
      * Constructs the 1d wave propagation solver.
      *
      * @param i_nXCells number of cells in x direction.
      * @param i_nYCells number of cells in y direction.
      **/
    WavePropagation2d( t_idx i_nXCells, 
                       t_idx i_nYCells );

    /**
      * Destructor which frees all allocated memory.
      **/
    ~WavePropagation2d(); 

    /**
      * Performs a time step.
      *
      * @param i_scaling scaling of the time step (dt / dx).
      * @param i_solv stand for solver Type 1 = Roe and 2 = F-wave
      **/
    void timeStep( t_real i_scaling,
                   unsigned short i_solv );

    /**
      * Sets the values of the ghost cells according to outflow boundary conditions.
      **/
    void setGhostOutflow();


    // TODO: add or remove these functions 
    /**
     * Sets the values of the ghost cells according to reflecting boundary conditions.
     **/
    void setGhostReflecting();
    
    /**
     * Sets the values of the ghost cells according to outflow on the left side 
     * and reflecting boundary on the left side.
     **/
    void setGhostOutflowReflecting();

    /**
     * Sets the values of the ghost cells according to reflecting boundary on the left side 
     * and outflow condition on the left side.
     **/
    void setGhostReflectingOutflow();

    /**
      * Gets the stride in y-direction. x-direction is stride-1.
      *
      * @return stride in y-direction.
      **/
    t_idx getStride(){
      return m_nXCells+2;
    }

    /**
      * Gets cells' water heights.
      *
      * @return water heights.
      */
    t_real const * getHeight(){
      return m_h[m_step];
    }
    /**
      * Gets cells' Bathymetry.
      *
      * @return Bathymetry.
      */
    t_real const * getBathymetry(){
      return m_b;
    }

    /**
      * Gets the cells' momenta in x-direction.
      *
      * @return momenta in x-direction.
      **/
    t_real const * getMomentumX(){
      return m_hu[m_step];
    }

    /**
      * Dummy function which returns a nullptr.
      **/
    t_real const * getMomentumY(){
      return  m_hv[m_step];
    }

    /**
      * Sets the height of the cell to the given value.
      *
      * @param i_ix id of the cell in x-direction.
      * @param i_iy id of the cell in y-direction.
      * @param i_h water height.
      **/
    void setHeight( t_idx  i_ix,
                    t_idx  i_iy,
                    t_real i_h ) {
      m_h[m_step][(m_nXCells + 2)*(i_iy+1)+ (i_ix+1)] = i_h;
    }

    /**
      * Sets the momentum in x-direction to the given value.
      *
      * @param i_ix id of the cell in x-direction.
      * @param i_iy id of the cell in y-direction.
      * @param i_hu momentum in x-direction.
      **/
    void setMomentumX( t_idx  i_ix,
                       t_idx  i_iy,
                       t_real i_hu ) {
      m_hu[m_step][(m_nXCells + 2)*(i_iy+1)+ (i_ix+1)] = i_hu;
    }

    /**
      * Sets the bathymetry of the cell to the given value.
      *
      * @param i_ix id of the cell in x-direction.
      * @param i_iy id of the cell in y-direction.
      * @param i_b bathymetry.
      **/
    void setBathymetry( t_idx  i_ix,
                        t_idx  i_iy,
                        t_real i_b) {
      m_b[(m_nXCells + 2)*(i_iy+1)+ (i_ix+1)] = i_b;
    }

    /**
      * Sets the momentum in x-direction to the given value.
      *
      * @param i_ix id of the cell in x-direction.
      * @param i_iy id of the cell in y-direction.
      * @param i_hv momentum in y-direction.
      **/
    void setMomentumY( t_idx  i_ix,
                       t_idx  i_iy,
                       t_real i_hv ) {
      m_hv[m_step][(m_nXCells + 2)*(i_iy+1)+ (i_ix+1)] = i_hv;
    }

};

#endif
