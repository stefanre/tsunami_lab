/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * F-Wave Riemann solver for the shallow water equations.
 **/
#include "Fwave.h"
#include <cmath>

void tsunami_lab::solvers::Fwave::waveSpeeds( t_real   i_hL,
                                              t_real   i_hR,
                                              t_real   i_uL,
                                              t_real   i_uR,
                                              t_real & o_waveSpeedL,
                                              t_real & o_waveSpeedR ) {
  // pre-compute square-root ops
  t_real l_hSqrtL = std::sqrt( i_hL );
  t_real l_hSqrtR = std::sqrt( i_hR );

  // compute Roe averages
  t_real l_hRoe = t_real(0.5) * ( i_hL + i_hR );
  t_real l_uRoe = l_hSqrtL * i_uL + l_hSqrtR * i_uR;
  l_uRoe /= l_hSqrtL + l_hSqrtR;

  // compute wave speeds
  t_real l_ghSqrtRoe = m_gSqrt * std::sqrt( l_hRoe );
  o_waveSpeedL = l_uRoe - l_ghSqrtRoe;
  o_waveSpeedR = l_uRoe + l_ghSqrtRoe;
}


void tsunami_lab::solvers::Fwave::deltaFluxFunciton( t_real   i_hL,
                                                      t_real   i_hR,
                                                      t_real   i_uL,
                                                      t_real   i_uR,
                                                      t_real   o_delta_flux_func[2]){
  // initialize flux function for the left and right side
  t_real l_fluxR[2] = {0};
  t_real l_fluxL[2] = {0};

  // compute h*u
  l_fluxR[0] = i_hR * i_uR;
  l_fluxL[0] = i_hL * i_uL;

  // compute hu² + 0.5*g*h²
  l_fluxR[1] = i_hR * i_uR*i_uR + t_real(0.5) * m_g * i_hR * i_hR;
  l_fluxL[1] = i_hL * i_uL*i_uL + t_real(0.5) * m_g * i_hL * i_hL;

  // compute flux function delta
  o_delta_flux_func[0] = l_fluxR[0] - l_fluxL[0];
  o_delta_flux_func[1] = l_fluxR[1] - l_fluxL[1];
}


void tsunami_lab::solvers::Fwave::alpha( t_real   i_lamda1,
                                         t_real   i_lamda2,
                                         t_real   i_deltaFlux[2],
                                         t_real   o_alpha[2]){

  // matrix indices
  // [0][0]   [1][0]
  // [0][1]   [1][1]                                     
  t_real eigenInv[2][2] = {0};
  t_real determinant = i_lamda2 - i_lamda1;

  eigenInv[0][0] = i_lamda2 / determinant;
  eigenInv[0][1] = (-1.0f * i_lamda1)/determinant;
  eigenInv[1][0] = -1.0f / determinant;
  eigenInv[1][1] = 1 / determinant;

  // compute eigenInv * deltaFlux 
  o_alpha[0] = eigenInv[0][0] * i_deltaFlux[0] + eigenInv[1][0] * i_deltaFlux[1];
  o_alpha[1] = eigenInv[0][1] * i_deltaFlux[0] + eigenInv[1][1] * i_deltaFlux[1];
}


void tsunami_lab::solvers::Fwave::netUpdates( t_real i_hL,
                                              t_real i_hR,
                                              t_real i_huL,
                                              t_real i_huR,
                                              t_real o_netUpdateL[2],
                                              t_real o_netUpdateR[2] ) {
  // compute particle velocities
  t_real l_uL = i_huL / i_hL;
  t_real l_uR = i_huR / i_hR;


  // calculation of eigenvectors rRoe
  t_real l_rRoe1[2] = {1, 0};
  t_real l_rRoe2[2] = {1, 0};
  t_real l_lamda1 = 0;
  t_real l_lamda2 = 0;

  waveSpeeds( i_hL,
              i_hR,
              l_uL,
              l_uR,
              l_lamda1,
              l_lamda2 );

  l_rRoe1[1] = l_lamda1;
  l_rRoe2[1] = l_lamda2;

  // compute flux function delta deltaf = f(qR) - f(qL)
  t_real l_deltaFlux[2] = {0};

  deltaFluxFunciton( i_hL,
                     i_hR,
                     l_uL,
                     l_uR,
                     l_deltaFlux );

  // compute alpha 1 and 2 
  t_real l_alpha[2] = {0};

  alpha( l_lamda1,
         l_lamda2,
         l_deltaFlux,
         l_alpha);


  // set net-update depending on eigenvalues
  
  o_netUpdateL[0] = 0;
  o_netUpdateR[0] = 0;
  o_netUpdateL[1] = 0;
  o_netUpdateR[1] = 0;

  if(l_lamda1 < 0){
    o_netUpdateL[0] = l_alpha[0] * l_rRoe1[0];
    o_netUpdateL[1] = l_alpha[0] * l_rRoe1[1];
  }
  else{
    o_netUpdateR[0] = l_alpha[0] * l_rRoe1[0];
    o_netUpdateR[1] = l_alpha[0] * l_rRoe1[1];
  }
  if(l_lamda2 < 0){
    o_netUpdateL[0] += l_alpha[1] * l_rRoe2[0];
    o_netUpdateR[1] += l_alpha[1] * l_rRoe2[1];
  }
  else{
    o_netUpdateR[0] += l_alpha[1] * l_rRoe2[0];
    o_netUpdateR[1] += l_alpha[1] * l_rRoe2[1];
  }
}

/*----------------------------------------------------------------------*/

void tsunami_lab::solvers::Fwave::deltaFluxFuncitonWbathymetry( t_real i_hL,
                                                                t_real i_hR,
                                                                t_real i_uL,
                                                                t_real i_uR,
                                                                t_real i_bL,
                                                                t_real i_bR,  
                                                                t_real o_delta_flux_func[2]){
  // initialize flux function for the left and right side
  t_real l_fluxR[2] = {0};
  t_real l_fluxL[2] = {0};

  // compute h*u
  l_fluxR[0] = i_hR * i_uR;
  l_fluxL[0] = i_hL * i_uL;

  // compute hu² + 0.5*g*h²
  l_fluxR[1] = i_hR * pow(i_uR,2) + t_real(0.5) * m_g * pow(i_hR,2);
  l_fluxL[1] = i_hL * pow(i_uL,2) + t_real(0.5) * m_g * pow(i_hL,2);

  // compute flux function delta
  o_delta_flux_func[0] = l_fluxR[0] - l_fluxL[0];
  o_delta_flux_func[1] = l_fluxR[1] - l_fluxL[1];

  // compute bathymetry term 
  t_real l_bathymetry = -m_g * ( i_bR - i_bL);
  l_bathymetry = l_bathymetry * t_real(0.5) * ( i_hL + i_hR );

  // subtract bathymetry from flux delta
  o_delta_flux_func[1] = o_delta_flux_func[1] - l_bathymetry;
}

  
void tsunami_lab::solvers::Fwave::netUpdatesWbathymetry( t_real i_hL,
                                                          t_real i_hR,
                                                          t_real i_huL,
                                                          t_real i_huR,
                                                          t_real i_bL,
                                                          t_real i_bR,
                                                          t_real o_netUpdateL[2],
                                                          t_real o_netUpdateR[2] ) {
    // compute particle velocities
    t_real l_uL = i_huL / i_hL;
    t_real l_uR = i_huR / i_hR;


    // calculation of eigenvectors rRoe
    t_real l_rRoe1[2] = {1, 0};
    t_real l_rRoe2[2] = {1, 0};
    t_real l_lamda1 = 0;
    t_real l_lamda2 = 0;

    waveSpeeds( i_hL,
                i_hR,
                l_uL,
                l_uR,
                l_lamda1,
                l_lamda2 );

    l_rRoe1[1] = l_lamda1;
    l_rRoe2[1] = l_lamda2;

    // compute flux function delta deltaf = f(qR) - f(qL)
    t_real l_deltaFlux[2] = {0};

    deltaFluxFuncitonWbathymetry( i_hL,
                                  i_hR,
                                  l_uL,
                                  l_uR,
                                  i_bL,
                                  i_bR,
                                  l_deltaFlux );

    // compute alpha 1 and 2 
    t_real l_alpha[2] = {0};

    alpha( l_lamda1,
          l_lamda2,
          l_deltaFlux,
          l_alpha);


    // set net-update depending on eigenvalues
    
    o_netUpdateL[0] = 0;
    o_netUpdateR[0] = 0;
    o_netUpdateL[1] = 0;
    o_netUpdateR[1] = 0;

    if(l_lamda1 < 0){
      o_netUpdateL[0] = l_alpha[0] * l_rRoe1[0];
      o_netUpdateL[1] = l_alpha[0] * l_rRoe1[1];
    }
    else{
      o_netUpdateR[0] = l_alpha[0] * l_rRoe1[0];
      o_netUpdateR[1] = l_alpha[0] * l_rRoe1[1];
    }
    if(l_lamda2 < 0){
      o_netUpdateL[0] += l_alpha[1] * l_rRoe2[0];
      o_netUpdateR[1] += l_alpha[1] * l_rRoe2[1];
    }
    else{
      o_netUpdateR[0] += l_alpha[1] * l_rRoe2[0];
      o_netUpdateR[1] += l_alpha[1] * l_rRoe2[1];
    }
}


