/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Unit tests of the F-Wave Riemann solver.
 **/
#include <catch2/catch.hpp>
#define private public
#include "Fwave.h"
#undef public

TEST_CASE( "Test the derivation of the Roe speeds. (F-Wave)", "[RoeSpeedsFwave]" ) {
   /*
    * Test case:
    *  h: 10 | 9
    *  u: -3 | 3
    *
    * roe height: 9.5
    * roe velocity: (sqrt(10) * -3 + 3 * 3) / ( sqrt(10) + sqrt(9) )
    *               = -0.0790021169691720
    * roe speeds: s1 = -0.079002116969172024 - sqrt(9.80665 * 9.5) = -9.7311093998375095
    *             s2 = -0.079002116969172024 + sqrt(9.80665 * 9.5) =  9.5731051658991654
    */
  float l_waveSpeedL = 0;
  float l_waveSpeedR = 0;
  tsunami_lab::solvers::Fwave::waveSpeeds( 10,
                                           9,
                                           -3,
                                           3,
                                           l_waveSpeedL,
                                           l_waveSpeedR );

  REQUIRE( l_waveSpeedL == Approx( -9.7311093998375095 ) );
  REQUIRE( l_waveSpeedR == Approx(  9.5731051658991654 ) );
}

TEST_CASE(" Test the calculation of Delta f", "[deltaFluxFunction]") {
    /*
     * Test case:
     *  h: 3  |  5
     *  u: -2 |  2
     *
     * right side: 5 * 2                       = 10
     *             5 * 2² + 0.5 * 9.80665 * 5² = 142.583125
     *
     * left side : 3 * (-2)                       = -6
     *             3 * (-2)² + 0.5 * 9.80665 * 3² = 56.129925
     *
     * delta f = f(qR) - f(qL)    
     * 
     * delta f =     10       -    (-6)     = 16
     *           142.583125   -  56.129925  = 86.4532
     */

     float l_deltaFlux[2] =  {0};
     tsunami_lab::solvers::Fwave::deltaFluxFunciton( 3,
                                                     5,
                                                     -2,
                                                     2,
                                                     l_deltaFlux );
    
    REQUIRE( l_deltaFlux[0] == 16);
    REQUIRE( l_deltaFlux[1] == 86.4532f);

}

TEST_CASE( " Test the calculation of alpha ", "[alpha]") {
    /*
     * Test case:
     *  lamda1 = -2.78
     *  lamda2 = 2.78
     *
     *  delta f = (  4.2   )
     *            ( 9.2123 )
     *
     * inverse matrix: (lamda2 - lamda1) * (  lamda2   -1 ) = (0.5   -0.179856115108)
     *   (inverse of 2x2)                  ( -lamda1    1 ) = (0.5    0.179856115108)
     *
     * alpha: (o.5   -0.179856115108) * (   4.2  ) = (0.443111510791)
     *        (0.5    0.179856115108)   ( 9.2123 )   (3.75688848921)
     */

    float l_deltaFlux[2] = {4.2, 9.2123};
    float l_alpha[2] = {0};

    tsunami_lab::solvers::Fwave::alpha( -2.78,
                                        2.78,
                                        l_deltaFlux,
                                        l_alpha);
    REQUIRE( l_alpha[0] == Approx(0.443111510791));
    REQUIRE( l_alpha[1] == Approx(3.75688848921));
     
}


TEST_CASE( "Test the derivation of the F-Wave net-updates.", "[UpdatesFwave]" ) {
  /*
   * Test case:
   *
   *      left | right
   *  h:    10 | 9
   *  u:    -3 | 3
   *  hu:  -30 | 27
   *
   * this calculation is already shown in the Roe.test.cpp file
   */
  float l_netUpdatesL[2] = { -5, 3 };
  float l_netUpdatesR[2] = {  4, 7 };

  tsunami_lab::solvers::Fwave::netUpdates( 10,
                                           9,
                                           -30,
                                           27,
                                           l_netUpdatesL,
                                           l_netUpdatesR );

  REQUIRE( l_netUpdatesL[0] == Approx( 33.5590017014261447899292 ) );
  REQUIRE( l_netUpdatesL[1] == Approx( -326.56631690591093200508 ) );

  REQUIRE( l_netUpdatesR[0] == Approx( 23.4409982985738561366777 ) );
  REQUIRE( l_netUpdatesR[1] == Approx( 224.403141905910928927533 ) );

  /*
   * Test case (dam break):
   *
   *     left | right
   *   h:  10 | 8
   *   hu:  0 | 0
   *
   * this calculation is already shown in the Roe.test.cpp file
   */
  tsunami_lab::solvers::Fwave::netUpdates( 10,
                                           8,
                                           0,
                                           0,
                                           l_netUpdatesL,
                                           l_netUpdatesR ); 

  REQUIRE( l_netUpdatesL[0] ==  Approx(9.394671362) );
  REQUIRE( l_netUpdatesL[1] == -Approx(88.25985)    );

  REQUIRE( l_netUpdatesR[0] == -Approx(9.394671362) );
  REQUIRE( l_netUpdatesR[1] == -Approx(88.25985)    );

  /*
   * Test case (trivial steady state):
   *
   *     left | right
   *   h:  10 | 10
   *  hu:   0 |  0
   */
  tsunami_lab::solvers::Fwave::netUpdates( 10,
                                           10,
                                           0,
                                           0,
                                           l_netUpdatesL,
                                           l_netUpdatesR );

  REQUIRE( l_netUpdatesL[0] == Approx(0) );
  REQUIRE( l_netUpdatesL[1] == Approx(0) );

  REQUIRE( l_netUpdatesR[0] == Approx(0) );
  REQUIRE( l_netUpdatesR[1] == Approx(0) );

  /*
   * Test case for supersonic problems
   *     left | right
   *    h:  1 |  0.2
   *   hu:  2 |  2
   * 
   * lamda1 = 2.04644223979
   * lamda2 = 6.89782967021
   *
   * delta f: (    0    )
   *          (11.292808)
   *
   * alpha1 = -2.32774812607
   * alpha2 =  2.32774812607
   *
   * Z_1 = ( -2.32774812607 )
   *       ( -4.76360208878 )
   *
   * Z_2 = ( 2.32774812607 )
   *       ( 16.0564100888 )
   *
   * Z_1 + Z_2 = (     0     )
   *             ( 11.292808 )
   */

   tsunami_lab::solvers::Fwave::netUpdates( 1,
                                            0.2,
                                            2,
                                            2,
                                            l_netUpdatesL,
                                            l_netUpdatesR );

  REQUIRE( l_netUpdatesL[0] == Approx(0) );
  REQUIRE( l_netUpdatesL[1] == Approx(0) );

  REQUIRE( l_netUpdatesR[0] == Approx(0) );
  REQUIRE( l_netUpdatesR[1] == Approx(11.292808) );
}

TEST_CASE(" Test the calculation of Delta f minus bathymetry", "[deltaFluxMinusBathymetry]") {
    /*
     * Test case:
     *  h: 3    |  5
     *  u: -2   |  2
     *  b: -1.5 |  -3.5
     *
     * right side: 5 * 2                       = 10
     *             5 * 2² + 0.5 * 9.80665 * 5² = 142.583125
     *
     * left side : 3 * (-2)                       = -6
     *             3 * (-2)² + 0.5 * 9.80665 * 3² = 56.129925
     *
     * delta f = f(qR) - f(qL)    
     * 
     * delta f =     10       -    (-6)     = 16
     *           142.583125   -  56.129925  = 86.4532
     * 
     * delta f - bathymetry =  16      -   0        = 16
     *                        86.4532  -   78.435   = 8
     */

     float l_deltaFlux[2] =  {0};
     tsunami_lab::solvers::Fwave::deltaFluxFuncitonWbathymetry( 3,
                                                                5,
                                                                -2,
                                                                2,
                                                                -1.5,
                                                                -3.5,
                                                                l_deltaFlux );
    
    REQUIRE( l_deltaFlux[0] == 16);
    REQUIRE( l_deltaFlux[1] == 8);

}
// TODO: do more tests!