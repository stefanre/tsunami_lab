/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * F-Wave Riemann solver for the shallow water equations.
 **/
#ifndef TSUNAMI_LAB_SOLVERS_FWAVE
#define TSUNAMI_LAB_SOLVERS_FWAVE

#include "../constants.h"

namespace tsunami_lab {
  namespace solvers {
    class Fwave;
  }
}

class tsunami_lab::solvers::Fwave {
  private:
    //! square root of gravity
    static t_real constexpr m_gSqrt = 3.131557121;
    //! gravity constant
    static t_real constexpr m_g = 9.80665;

    /**
     * Computes the wave speeds.
     *
     * @param i_hL height of the left side.
     * @param i_hR height of the right side.
     * @param i_uL particle velocity of the leftside.
     * @param i_uR particles velocity of the right side.
     * @param o_waveSpeedL will be set to the speed of the wave propagating to the left.
     * @param o_waveSpeedR will be set to the speed of the wave propagating to the right.
     **/
    static void waveSpeeds( t_real   i_hL,
                            t_real   i_hR,
                            t_real   i_uL,
                            t_real   i_uR,
                            t_real & o_waveSpeedL,
                            t_real & o_waveSpeedR );


    /**
     * Computes the flux function delta.
     *
     * @param i_hL height of the left side.
     * @param i_hR height of the right side.
     * @param i_uL particle velocity of the leftside.
     * @param i_uR particles velocity of the right side.
     * @param o_delta_flux_func will be set to the delta of the flux function.
     **/
    static void deltaFluxFunciton( t_real   i_hL,
                                   t_real   i_hR,
                                   t_real   i_uL,
                                   t_real   i_uR,
                                   t_real   o_delta_flux_func[2]);


    /**
     * Computes the two alpha values.
     *
     * @param i_lamda1 first eigenvalue.
     * @param i_lamda2 second eigenvalue
     * @param i_deltaFlux delta of the flux fuction = f(uR) - f(uL).
     * @param o_alpha will be set to two alpha values needed for the net-updates.
     **/
    static void alpha( t_real   i_lamda1,
                       t_real   i_lamda2,
                       t_real   i_deltaFlux[2],
                       t_real   o_alpha[2]); 

    /**
     * Computes the flux function delta.
     *
     * @param i_hL height of the left side.
     * @param i_hR height of the right side.
     * @param i_uL particle velocity of the leftside.
     * @param i_uR particles velocity of the right side.
     * @param i_bL bathymetry of the left side.
     * @param i_bR bathymetry of the right side.
     * @param o_delta_flux_func will be set to the delta of the flux function minus bathymetry.
     **/
    static void deltaFluxFuncitonWbathymetry( t_real i_hL,
                                                t_real i_hR,
                                                t_real i_uL,
                                                t_real i_uR,
                                                t_real i_bL,
                                                t_real i_bR,  
                                                t_real o_delta_flux_func[2]);
  public:
    /**
     * Computes the net-updates.
     *
     * @param i_hL height of the left side.
     * @param i_hR height of the right side.
     * @param i_huL momentum of the left side.
     * @param i_huR momentum of the right side.
     * @param o_netUpdateL will be set to the net-updates for the left side; 0: height, 1: momentum.
     * @param o_netUpdateR will be set to the net-updates for the right side; 0: height, 1: momentum.
     **/
    static void netUpdates( t_real i_hL,
                            t_real i_hR,
                            t_real i_huL,
                            t_real i_huR,
                            t_real o_netUpdateL[2],
                            t_real o_netUpdateR[2] );

    /**
     * Computes the net-updates. with support for bathymetry
     *
     * @param i_hL height of the left side.
     * @param i_hR height of the right side.
     * @param i_huL momentum of the left side.
     * @param i_huR momentum of the right side.
     * @param o_netUpdateL will be set to the net-updates for the left side; 0: height, 1: momentum.
     * @param o_netUpdateR will be set to the net-updates for the right side; 0: height, 1: momentum.
     **/
    static void netUpdatesWbathymetry( t_real i_hL,
                                       t_real i_hR,
                                       t_real i_huL,
                                       t_real i_huR,
                                       t_real i_bL,
                                       t_real i_bR,
                                       t_real o_netUpdateL[2],
                                       t_real o_netUpdateR[2] );
};


#endif