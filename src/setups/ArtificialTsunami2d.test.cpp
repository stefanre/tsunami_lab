/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Test the 2D circular dam break setup.
 */
#include <catch2/catch.hpp>
#include "ArtificialTsunami2d.h"

TEST_CASE( "Test the ArtificialTsunami2d setup.", "[ArtificialTsunami2d]" ) {
  tsunami_lab::setups::ArtificialTsunami2d l_example;


  REQUIRE( l_example.getHeight( 45, 50.12 ) == 100 );

  REQUIRE( l_example.getMomentumX( 45, 50.12 ) == 0 );

  REQUIRE( l_example.getMomentumY( 45, 59.12 ) == 0 );

  REQUIRE( l_example.getBathymetry(45, 50.12 ) == -100);

 
  REQUIRE( l_example.getHeight( 5000 , 5000 ) == 100 );

  REQUIRE( l_example.getMomentumX( 5000 , 5000 ) == 0);

  REQUIRE( l_example.getMomentumY( 5000 , 5000 ) == 0 );

  REQUIRE( l_example.getBathymetry( 5011 ,5102 ) == Approx(-100.330930118));

 
}