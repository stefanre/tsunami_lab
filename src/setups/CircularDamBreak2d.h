/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Two-dimensional example to show effect of bathymetry
 **/
#ifndef TSUNAMI_LAB_SETUPS_CIRCULAR_DAM_BREAK_2D_H
#define TSUNAMI_LAB_SETUPS_CIRCULAR_DAM_BREAK_2D_H

#include "Setup.h"

namespace tsunami_lab {
  namespace setups {
    class CircularDamBreak2d;
  }
}

/**
 * Two-dimensional example of circular dam break.
 **/
class tsunami_lab::setups::CircularDamBreak2d: public Setup {
  private:

    //! height of the dam
    t_real m_heightDam = 0;

    //! height of the water around the dam
    t_real m_heightLake = 0;

  public:
    /**
     * Constructor.
     *
     * @param i_momentumLeft momentum on the left side.
     * @param i_momentumRight momentum on the right side.
     * @param i_locationDam location (x-coordinate) of the dam under water.
     **/
    CircularDamBreak2d( t_real i_heightDam,
                        t_real i_heightLake );


    /**
     * Gets the water height at a given point.
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return height at the given point.
     **/
    t_real getHeight( t_real i_x,
                      t_real i_y ) const;

    /**
     * Gets the momentum in x-direction.
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return momentum in x-direction.
     **/
    t_real getMomentumX( t_real i_x,
                         t_real i_y ) const;

    /**
     * Gets the momentum in y-direction.
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return momentum in y-direction.
     **/
    t_real getMomentumY( t_real i_x,
                         t_real i_y ) const;

    /**
     *
     * Gets the bathymetry at a given point
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return bathymetry at the given point.
     **/
     t_real getBathymetry( t_real i_x,
                           t_real i_y ) const;

};

#endif