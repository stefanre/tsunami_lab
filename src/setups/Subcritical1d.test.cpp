/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Tests the subcritical setup.
 **/
#include <catch2/catch.hpp>
#include "Subcritical1d.h"

TEST_CASE( "Test the subcritical setup.", "[Subcritical1d]" ) {
  tsunami_lab::setups::Subcritical1d l_subcritical( 4.42 );

  // strade points
  REQUIRE( l_subcritical.getHeight( 2, 0 ) == 2 );

  REQUIRE( l_subcritical.getMomentumX( 2, 0 ) == 4.42f );

  REQUIRE( l_subcritical.getMomentumY( 2, 0 ) == 0 );

  REQUIRE( l_subcritical.getBathymetry( 2, 0) == -2 );

  REQUIRE( l_subcritical.getHeight( 2, 5 ) == 2 );

  REQUIRE( l_subcritical.getMomentumX( 2, 5 ) == 4.42f );

  REQUIRE( l_subcritical.getMomentumY( 2, 2 ) == 0 );

  REQUIRE( l_subcritical.getBathymetry( 2, 2) == -2 );


  // on the curve
  REQUIRE( l_subcritical.getHeight( 10, 0 ) == 1.8f );

  REQUIRE( l_subcritical.getMomentumX( 10, 0 ) == 4.42f );

  REQUIRE( l_subcritical.getMomentumY( 10, 0 ) == 0 );

  REQUIRE( l_subcritical.getBathymetry( 10, 0) == - 1.8f );

  REQUIRE( l_subcritical.getHeight( 10, 5 ) == 1.8f );

  REQUIRE( l_subcritical.getMomentumX( 10, 5 ) == 4.42f );

  REQUIRE( l_subcritical.getMomentumY( 10, 2 ) == 0 );  

  REQUIRE( l_subcritical.getBathymetry( 10, 5) == - 1.8f );
}