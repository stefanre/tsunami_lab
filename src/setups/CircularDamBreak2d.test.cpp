/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Test the 2D circular dam break setup.
 */
#include <catch2/catch.hpp>
#include "CircularDamBreak2d.h"

TEST_CASE( "Test the CircularDamBreak2d setup.", "[CircularDamBreak2d]" ) {
  tsunami_lab::setups::CircularDamBreak2d l_example( 10,
                                                     5 );

  // middle of the dam
  REQUIRE( l_example.getHeight( 45, 50.12 ) == 20 );

  REQUIRE( l_example.getMomentumX( 45, 50.12 ) == 0 );

  REQUIRE( l_example.getMomentumY( 45, 59.12 ) == 0 );

  REQUIRE( l_example.getBathymetry(45, 50.12 ) == -7);

  // right side
  REQUIRE( l_example.getHeight( 3, -12.9 ) == 7 );

  REQUIRE( l_example.getMomentumX( 3, -12.9 ) == 0);

  REQUIRE( l_example.getMomentumY( 3, -12.9 ) == 0 );

  REQUIRE( l_example.getBathymetry( 3, -12.9 ) == -7);

 
}