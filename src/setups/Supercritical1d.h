/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * One-dimensional example to show Supercritical flows
 **/
#ifndef TSUNAMI_LAB_SETUPS_SUPERCRITICAL_1D_H
#define TSUNAMI_LAB_SETUPS_SUPERCRITICAL_1D_H

#include "Setup.h"

namespace tsunami_lab {
  namespace setups {
    class Supercritical1d;
  }
}

/**
 * 1d example setup.
 **/
class tsunami_lab::setups::Supercritical1d: public Setup {
  private:

    //! momentum in the flow.
    t_real m_momentum = 0;
        
  public:
    /**
     * Constructor.
     *
     * @param i_momentum momentum in the flow.
     **/
    Supercritical1d( t_real i_momentum );


    /**
     * Gets the water height at a given point.
     *
     * @param i_x x-coordinate of the queried point.
     * @return height at the given point.
     **/
    t_real getHeight( t_real i_x,
                      t_real      ) const;

    /**
     * Gets the momentum in x-direction.
     *
     * @return momentum in x-direction.
     **/
    t_real getMomentumX( t_real ,
                         t_real ) const;

    /**
     * Gets the momentum in y-direction.
     *
     * @return momentum in y-direction.
     **/
    t_real getMomentumY( t_real,
                         t_real ) const;

    /**
     *
     * Gets the bathymetry at a given point
     *
     * @param i_x x-coordinate of the queried point.
     * @return bathymetry at the given point.
     **/
     t_real getBathymetry( t_real i_x,
                           t_real) const;

};

#endif