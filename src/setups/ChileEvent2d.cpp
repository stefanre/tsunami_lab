/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Two-dimensional Tsunami2d setup in a pool 
 **/
 #include "ChileEvent2d.h"
 #include "../io/NetCdf.h"
 #include <cmath>

tsunami_lab::setups::ChileEvent2d::ChileEvent2d(){
  m_bathX = new t_real[m_xB];
  m_bathY = new t_real[m_yB];
  m_bathData = new t_real[ m_xB * m_yB ];

  m_dispX = new t_real[m_xD];
  m_dispY = new t_real[m_yD];
  m_dispData = new t_real[ m_xD * m_yD ];

  // initialize arrays
  // bathymetry
  tsunami_lab::io::NetCdf::read(m_inputFileB, "x", m_bathX);
  tsunami_lab::io::NetCdf::read(m_inputFileB, "y", m_bathY);
  tsunami_lab::io::NetCdf::read(m_inputFileB, "z", m_bathData);

  // displacement
  tsunami_lab::io::NetCdf::read(m_inputFileD, "x", m_dispX);
  tsunami_lab::io::NetCdf::read(m_inputFileD, "y", m_dispY);
  tsunami_lab::io::NetCdf::read(m_inputFileD, "z", m_dispData);

}

tsunami_lab::setups::ChileEvent2d::~ChileEvent2d(){
  delete[] m_bathX ;
  delete[] m_bathY ;
  // hier muss noch der Fehler gefunden werden
  //delete m_bathData ;
  delete[] m_dispX;
  delete[] m_dispY;
  delete[] m_dispData;

}

tsunami_lab::t_real tsunami_lab::setups::ChileEvent2d::getHeight( t_real i_x,
                                                                  t_real i_y ) const {

  t_real l_delta = 20;                                                                        
  // get the  nearest index
  t_idx l_x = getNearestIndex(m_bathX, m_xB, i_x);
  t_idx l_y = getNearestIndex(m_bathY, m_yB, i_y);

  t_real l_height = - m_bathData[l_x + l_y*m_xB];

  if( l_height <= 0 ){
    return 0;
  } else {
    if(l_height < l_delta ){
      return l_delta;
    } else {
      return l_height;
    }
  }
}

tsunami_lab::t_real tsunami_lab::setups::ChileEvent2d::getMomentumX( t_real,
                                                                       t_real) const {
  return 0;
}

tsunami_lab::t_real tsunami_lab::setups::ChileEvent2d::getMomentumY( t_real,
                                                                       t_real) const {
  return 0;
}

tsunami_lab::t_real tsunami_lab::setups::ChileEvent2d::getBathymetry( t_real i_x,
                                                                      t_real i_y) const {
  t_real l_delta = 20;                                                                        
  // get the  nearest index
  t_idx l_x = getNearestIndex(m_bathX, m_xB, i_x);
  t_idx l_y = getNearestIndex(m_bathY, m_yB, i_y);

  t_real l_bathymetry = m_bathData[l_x + l_y*m_xB];

  // check if the value is in bounds of displacement data
  t_real l_displacement = 0;
  if(i_x >= m_dispX[0] && i_y >= m_dispY[0] && i_x <= m_dispX[m_xD-1] && i_y <= m_dispY[m_yD-1]){
    l_x = getNearestIndex(m_dispX, m_xD, i_x);
    l_y = getNearestIndex(m_dispY, m_yD, i_y);

    l_displacement = m_dispData[l_x + l_y*m_xD];
  }
  // return l_bathymetry + l_displacement;
  if(l_bathymetry < 0){
    if((l_bathymetry + l_displacement) > - l_delta){
      return - l_delta;
    } else {
      return l_bathymetry + l_displacement;
      }
    } else {
      if( l_bathymetry < l_delta ){
        return l_delta;
      } else {
        return l_bathymetry;
        }
  }
  

}

tsunami_lab::t_real tsunami_lab::setups::ChileEvent2d::getNearestIndex( t_real * i_array,
                                                                        t_idx    i_arraySize,
                                                                        t_real   i_pos){
  if( i_array == nullptr){
    std::cerr << "nullptr übergeben!" << std::endl;
    return -1;
  }
  t_idx nearestIndex = 0;
  for( t_idx l_c = 0; l_c < i_arraySize; l_c++){
    if(i_array[l_c] > i_pos){
      if( l_c > 0 && abs( i_array[l_c] - i_pos) > abs( i_array[l_c-1] - i_pos) ){
        nearestIndex = l_c-1;
      } else {
        nearestIndex = l_c;
      }
      break;
    }
  }
  return nearestIndex;
}