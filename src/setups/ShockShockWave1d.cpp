/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * One-dimensional shock-shock-wave Problem.
 **/
 #include "ShockShockWave1d.h"

 tsunami_lab::setups::ShockShockWave1d::ShockShockWave1d( t_real i_heightLeft,
                                                          t_real i_heightRight,
                                                          t_real i_momentum,
                                                          t_real i_locationSmash ){
  m_heightLeft = i_heightLeft;
  m_heightRight = i_heightRight;
  m_momentumLeft = i_momentum;
  m_momentumRight = t_real(-1) * i_momentum;
  m_locationSmash = i_locationSmash;
}

tsunami_lab::t_real tsunami_lab::setups::ShockShockWave1d::getHeight( t_real i_x,
                                                                      t_real      ) const {
  if( i_x < m_locationSmash ) {
    return m_heightLeft;
  }
  else {
    return m_heightRight;
  }
}

tsunami_lab::t_real tsunami_lab::setups::ShockShockWave1d::getMomentumX( t_real i_x,
                                                                         t_real     ) const {
  if( i_x < m_locationSmash ) {
    return m_momentumLeft;
  }
  else {
    return m_momentumRight;
  }
}

tsunami_lab::t_real tsunami_lab::setups::ShockShockWave1d::getMomentumY( t_real,
                                                                         t_real ) const {
  return 0;
}

tsunami_lab::t_real tsunami_lab::setups::ShockShockWave1d::getBathymetry( t_real i_x,
                                                                       t_real ) const {
  if(i_x > 4 && i_x < 6 ){
    return 0;
  }
  else {
    return 0;
  }

}