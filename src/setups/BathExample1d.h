/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * One-dimensional example to show effect of bathymetry
 **/
#ifndef TSUNAMI_LAB_SETUPS_BATH_EXAMPLE_1D_H
#define TSUNAMI_LAB_SETUPS_BATH_EXAMPLE_1D_H

#include "Setup.h"

namespace tsunami_lab {
  namespace setups {
    class BathExample1d;
  }
}

/**
 * 1d example setup.
 **/
class tsunami_lab::setups::BathExample1d: public Setup {
  private:
    
    //! momentum on the left side 
    t_real m_momentumLeft = 0;
        
    //! momentum on the right side
    t_real m_momentumRight = 0;

    //! location of the dam under water
    t_real m_locationDam = 0;

  public:
    /**
     * Constructor.
     *
     * @param i_momentumLeft momentum on the left side.
     * @param i_momentumRight momentum on the right side.
     * @param i_locationDam location (x-coordinate) of the dam under water.
     **/
    BathExample1d( t_real i_momentumLeft,
                   t_real i_momentumRight,
                   t_real i_locationDam );


    /**
     * Gets the water height at a given point.
     *
     * @param i_x x-coordinate of the queried point.
     * @return height at the given point.
     **/
    t_real getHeight( t_real i_x,
                      t_real      ) const;

    /**
     * Gets the momentum in x-direction.
     *
     * @return momentum in x-direction.
     **/
    t_real getMomentumX( t_real i_x,
                         t_real ) const;

    /**
     * Gets the momentum in y-direction.
     *
     * @return momentum in y-direction.
     **/
    t_real getMomentumY( t_real,
                         t_real ) const;

    /**
     *
     * Gets the bathymetry at a given point
     *
     * @param i_x x-coordinate of the queried point.
     * @return bathymetry at the given point.
     **/
     t_real getBathymetry( t_real i_x,
                           t_real) const;

};

#endif