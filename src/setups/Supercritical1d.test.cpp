/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Tests the Supercritical setup.
 **/
#include <catch2/catch.hpp>
#include "Supercritical1d.h"

TEST_CASE( "Test the Supercritical setup.", "[Supercritical1d]" ) {
  tsunami_lab::setups::Supercritical1d l_supercritical( 0.18 );

  // strade points
  REQUIRE( l_supercritical.getHeight( 2, 0 ) == 0.33f );

  REQUIRE( l_supercritical.getMomentumX( 2, 0 ) == 0.18f );

  REQUIRE( l_supercritical.getMomentumY( 2, 0 ) == 0 );

  REQUIRE( l_supercritical.getBathymetry( 2, 0) == -0.33f );

  REQUIRE( l_supercritical.getHeight( 2, 5 ) == 0.33f );

  REQUIRE( l_supercritical.getMomentumX( 2, 5 ) == 0.18f );

  REQUIRE( l_supercritical.getMomentumY( 2, 2 ) == 0 );

  REQUIRE( l_supercritical.getBathymetry( 2, 2) == -0.33f );


  // on the curve
  REQUIRE( l_supercritical.getHeight( 10, 0 ) == 0.13f );

  REQUIRE( l_supercritical.getMomentumX( 10, 0 ) == 0.18f );

  REQUIRE( l_supercritical.getMomentumY( 10, 0 ) == 0 );

  REQUIRE( l_supercritical.getBathymetry( 10, 0) == -0.13f );

  REQUIRE( l_supercritical.getHeight( 10, 5 ) == 0.13f );

  REQUIRE( l_supercritical.getMomentumX( 10, 5 ) == 0.18f );

  REQUIRE( l_supercritical.getMomentumY( 10, 2 ) == 0 );  

  REQUIRE( l_supercritical.getBathymetry( 10, 5) == -0.13f );
}