/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Test the bathymetry example.
 */
#include <catch2/catch.hpp>
#include "BathExample1d.h"

TEST_CASE( "Test the BathExample1d setup.", "[BathExample1d]" ) {
  tsunami_lab::setups::BathExample1d l_example( 3,
                                                3,
                                                5 );

  // left side
  REQUIRE( l_example.getHeight( 2, 0 ) == 8 );

  REQUIRE( l_example.getMomentumX( 2, 0 ) == 3 );

  REQUIRE( l_example.getMomentumY( 2, 0 ) == 0 );

  REQUIRE( l_example.getBathymetry(2,0 ) == -8);

  REQUIRE( l_example.getHeight( 2, 5 ) == 8 );

  REQUIRE( l_example.getMomentumX( 2, 5 ) == 3 );

  REQUIRE( l_example.getMomentumY( 2, 5 ) == 0 );

  REQUIRE( l_example.getBathymetry(2, 5 ) == -8);

  // right side
  REQUIRE( l_example.getHeight( 19, 0 ) == 4 );

  REQUIRE( l_example.getMomentumX( 19, 0 ) == 3);

  REQUIRE( l_example.getMomentumY( 19, 0 ) == 0 );

  REQUIRE( l_example.getBathymetry(19 ,0 ) == -4);

  REQUIRE( l_example.getHeight( 19, 5 ) == 4 );

  REQUIRE( l_example.getMomentumX( 19, 5 ) == 3 );

  REQUIRE( l_example.getMomentumY( 19, 2 ) == 0 ); 

  REQUIRE( l_example.getBathymetry(19 ,2 ) == -4);
}