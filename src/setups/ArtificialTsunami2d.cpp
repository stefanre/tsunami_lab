/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Two-dimensional Artificial Tsunami2d setup
 **/
 #include "ArtificialTsunami2d.h"
 #include <cmath>

tsunami_lab::setups::ArtificialTsunami2d::ArtificialTsunami2d(){}

tsunami_lab::t_real tsunami_lab::setups::ArtificialTsunami2d::getHeight( t_real ,
                                                                         t_real ) const {

  return 100;
}

tsunami_lab::t_real tsunami_lab::setups::ArtificialTsunami2d::getMomentumX( t_real,
                                                                            t_real) const {
  return 0;
}

tsunami_lab::t_real tsunami_lab::setups::ArtificialTsunami2d::getMomentumY( t_real,
                                                                            t_real) const {
  return 0;
}

tsunami_lab::t_real tsunami_lab::setups::ArtificialTsunami2d::getBathymetry( t_real i_x,
                                                                             t_real i_y) const {
  t_real l_x = i_x - 5000;
  t_real l_y = i_y - 5000;
  t_real l_fx = sin( ( ( l_x / 500) + 1) * M_PI );
  t_real l_gy = - (( l_y / 500 ) * ( l_y / 500 ) ) + 1;
  t_real l_d = 5 * l_fx * l_gy;

  if( i_x >= 4500 && i_x <= 5500 && i_y >= 4500 && i_y <= 5500 ){
    return -100 + l_d ;
  } else{
    return -100;
  }

}