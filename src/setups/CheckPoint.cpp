/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Setup to continue from a checkpoint.
 **/
 #include "CheckPoint.h"
 #include "../io/NetCdf.h"
 #include <cmath>

tsunami_lab::setups::CheckPoint::CheckPoint( t_idx i_x,
                                             t_idx i_y ){
  
  m_x = i_x;
  m_y = i_y;
  m_time = tsunami_lab::io::NetCdf::getDimLength( m_inputFile, "time");
  std::cout << "use existing checkpoint" << std::endl;
  std::cout << "ausgelesene zeitstempel: " << m_time << std::endl;
  // TODO continue from the time were the program stopped
  // t_real * l_timeArray = new t_real[m_time]; 
  // tsunami_lab::io::NetCdf::read(m_inputFile,"time", l_timeArray);
  // std::cout << "ausgelesene zeitstempel: " << l_timeArray[m_time-1] << std::endl;
  // m_time = l_timeArray[m_time-1];
  
  m_XArray = new t_real[m_x];
  m_YArray = new t_real[m_y];
  m_bathData = new t_real[ m_x * m_y ];
  m_hData = new t_real[ m_x * m_y ];
  m_huData = new t_real[ m_x * m_y ];
  m_hvData = new t_real[ m_x * m_y ];
  

  // initialize arrays
  tsunami_lab::io::NetCdf::read(m_inputFile, "x", m_XArray);
  tsunami_lab::io::NetCdf::read(m_inputFile, "y", m_YArray);
  tsunami_lab::io::NetCdf::read(m_inputFile, "b", m_bathData);
  tsunami_lab::io::NetCdf::read(m_inputFile, "h", m_hData);
  tsunami_lab::io::NetCdf::read(m_inputFile, "hu", m_huData);
  tsunami_lab::io::NetCdf::read(m_inputFile, "hv", m_hvData);

  

}

tsunami_lab::setups::CheckPoint::~CheckPoint(){
  delete[] m_XArray;
  delete[] m_YArray;
  delete[] m_bathData;
  delete[] m_hData;
  delete[] m_huData;
  delete[] m_hvData;

}

tsunami_lab::t_real tsunami_lab::setups::CheckPoint::getHeight( t_real i_x,
                                                                t_real i_y ) const {

  // get the  nearest index
  t_idx l_x = getNearestIndex(m_XArray, m_x, i_x);
  t_idx l_y = getNearestIndex(m_YArray, m_y, i_y);

  return m_hData[l_x + l_y*m_x];

}

tsunami_lab::t_real tsunami_lab::setups::CheckPoint::getMomentumX( t_real i_x,
                                                                   t_real i_y) const {
  t_idx l_x = getNearestIndex(m_XArray, m_x, i_x);
  t_idx l_y = getNearestIndex(m_YArray, m_y, i_y);

  return m_huData[l_x + l_y*m_x];
}

tsunami_lab::t_real tsunami_lab::setups::CheckPoint::getMomentumY( t_real i_x,
                                                                   t_real i_y) const {
  t_idx l_x = getNearestIndex(m_XArray, m_x, i_x);
  t_idx l_y = getNearestIndex(m_YArray, m_y, i_y);

  return m_hvData[l_x + l_y*m_x];
}

tsunami_lab::t_real tsunami_lab::setups::CheckPoint::getBathymetry( t_real i_x,
                                                                    t_real i_y) const {
  t_idx l_x = getNearestIndex(m_XArray, m_x, i_x);
  t_idx l_y = getNearestIndex(m_YArray, m_y, i_y);

  return m_bathData[l_x + l_y*m_x];

}

tsunami_lab::t_real tsunami_lab::setups::CheckPoint::getNearestIndex( t_real * i_array,
                                                                      t_idx    i_arraySize,
                                                                      t_real   i_pos){
  if( i_array == nullptr){
    std::cerr << "nullptr übergeben!" << std::endl;
    return -1;
  }
  t_idx nearestIndex = 0;
  for( t_idx l_c = 0; l_c < i_arraySize; l_c++){
    if(i_array[l_c] > i_pos){
      if( l_c > 0 && abs( i_array[l_c] - i_pos) > abs( i_array[l_c-1] - i_pos) ){
        nearestIndex = l_c-1;
      } else {
        nearestIndex = l_c;
      }
      break;
    }
  }
  return nearestIndex;
}