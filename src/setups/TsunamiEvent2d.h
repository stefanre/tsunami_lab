/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Two-dimensional Tsunami2d setup with netcdf data
 **/
#ifndef TSUNAMI_LAB_SETUPS_TSUNAMI_EVENT_2D_H
#define TSUNAMI_LAB_SETUPS_TSUNAMI_EVENT_2D_H

#include "Setup.h"

namespace tsunami_lab {
  namespace setups {
    class TsunamiEvent2d;
  }
}

/**
 * Two-dimensional Tsunami2d setup with netcdf data.
 **/
class tsunami_lab::setups::TsunamiEvent2d: public Setup {

  private:

    /**
      * Gets the nearest value inside the array
      *
      * @param i_array array of values ordered in ascending order
      * @param i_arraySize size ouf the input array
      * @param i_pos value for which the next array entry should be found
      **/  
    static t_real getNearestIndex( t_real * i_array,
                                 t_idx    i_arraySize,
                                 t_real   i_pos);

  public:

    //! number of x values for the bathymetry file
    t_idx m_xB = 1000;
    
    //! number of y values for the bathymetry file
    t_idx m_yB = 1000;

    //! number of x and y values for the displacement file.
    t_idx m_xyD = 100;

    float * m_bathX = nullptr;
    
    float * m_bathY = nullptr;

    float * m_bathData = nullptr;

    float * m_dispX = nullptr;

    float * m_dispY = nullptr;

    float * m_dispData = nullptr;

    //! name of the bathymetry input file
    const char * m_inputFileB = "inputData/artificialtsunami_bathymetry_1000.nc";

    //! name of the displacements input file
    const char * m_inputFileD = "inputData/artificialtsunami_displ_1000.nc";


    /**
     * Constructor.
     *
     **/
    TsunamiEvent2d();

    /**
      * Destructor which frees all allocated memory.
      */
    ~TsunamiEvent2d();

    /**
     * Gets the water height at a given point.
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return height at the given point.
     **/
    t_real getHeight( t_real ,
                      t_real  ) const;

    /**
     * Gets the momentum in x-direction.
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return momentum in x-direction.
     **/
    t_real getMomentumX( t_real i_x,
                         t_real i_y ) const;

    /**
     * Gets the momentum in y-direction.
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return momentum in y-direction.
     **/
    t_real getMomentumY( t_real i_x,
                         t_real i_y ) const;

    /**
     *
     * Gets the bathymetry at a given point
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return bathymetry at the given point.
     **/
     t_real getBathymetry( t_real i_x,
                           t_real i_y ) const;

};

#endif