/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Tests the TsunamiEvent1d setup.
 **/
#include <catch2/catch.hpp>
#include "TsunamiEvent1d.h"

TEST_CASE( "Test the TsunamiEvent1d setup.", "[TsunamiEvent1d]" ) {
  tsunami_lab::setups::TsunamiEvent1d l_tsunami1d( 0.0 );

  // test delta correction
  REQUIRE( l_tsunami1d.getHeight( 2, 0 ) == 20);

  REQUIRE( l_tsunami1d.getMomentumX( 2, 0 ) == 0 );

  REQUIRE( l_tsunami1d.getMomentumY( 2, 0 ) == 0 );

  REQUIRE( l_tsunami1d.getBathymetry( 2, 0) == -20 );

  REQUIRE( l_tsunami1d.getHeight( 2, 5 ) == 20 );

  REQUIRE( l_tsunami1d.getMomentumX( 2, 5 ) == 0 );

  REQUIRE( l_tsunami1d.getMomentumY( 2, 2 ) == 0 );

  REQUIRE( l_tsunami1d.getBathymetry( 2, 2) == -20 );


  // on the curve
  REQUIRE( l_tsunami1d.getHeight( 109, 0 ) ==  Approx(133.292863611));

  REQUIRE( l_tsunami1d.getMomentumX( 109, 0 ) == 0);

  REQUIRE( l_tsunami1d.getMomentumY( 109, 0 ) == 0 );

  REQUIRE( l_tsunami1d.getBathymetry( 109, 0) == Approx(-133.292863611) );

  REQUIRE( l_tsunami1d.getHeight( 109, 5 ) == Approx(133.292863611) );

  REQUIRE( l_tsunami1d.getMomentumX( 109, 5 ) == 0 );

  REQUIRE( l_tsunami1d.getMomentumY( 109, 2 ) == 0 );  

  REQUIRE( l_tsunami1d.getBathymetry( 109, 5) == Approx(-133.292863611) );
}