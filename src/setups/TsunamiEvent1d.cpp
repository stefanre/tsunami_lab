/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * One-dimensional example to show tsunami near japan.
 * Only usable in this specific case
 **/
#include "TsunamiEvent1d.h"
#include <vector>
#include "../io/Csv.h"
#include <cmath>

tsunami_lab::setups::TsunamiEvent1d::TsunamiEvent1d( t_real i_momentum ) {

  m_momentum = i_momentum;

  tsunami_lab::io::Csv::readTsunami1d( m_bathymetry );

}

tsunami_lab::t_real tsunami_lab::setups::TsunamiEvent1d::getHeight( t_real i_x,
                                                               t_real      ) const {
  // calculate height from bathymetry without displacement
  t_real l_tmp_bath = m_bathymetry[int(i_x)];
  if (i_x <= (m_dataSize - 1) && l_tmp_bath < 0){
        if ( l_tmp_bath > (- m_delta)){
            return m_delta;
        }
        else{
            return t_real(- l_tmp_bath);
        }
    } else{
        return 0.0;
    }

}

tsunami_lab::t_real tsunami_lab::setups::TsunamiEvent1d::getMomentumX( t_real ,
                                                                  t_real ) const {
  return 0;
}

tsunami_lab::t_real tsunami_lab::setups::TsunamiEvent1d::getMomentumY( t_real,
                                                                  t_real ) const {
  return 0;
}

tsunami_lab::t_real tsunami_lab::setups::TsunamiEvent1d::getBathymetry( t_real i_x,
                                                                        t_real ) const {
  // return value
  t_real l_bath = 0;
  // temporare value of bathymetry
  t_real l_tmp_bath = m_bathymetry[int(i_x)];
  
  // calculate the right bathymetrie
  if (i_x <= (int(m_dataSize) - 1) ){
      if( l_tmp_bath > (- m_delta) && l_tmp_bath < 0){
          l_bath = -m_delta;
      } else if( l_tmp_bath < m_delta && l_tmp_bath > 0){
        l_bath = m_delta;
      }
      else{
          l_bath = l_tmp_bath;
      }
  } else{
       l_bath = 0.0;
  }
  // calculate d(x)
  t_real l_displacement = ((i_x * 250) -175000.0)/37500;
  l_displacement = sin( l_displacement * M_PI + M_PI);
  l_displacement *= 10 ;
  // add d(x) to bathymetry
  if( (i_x * 250) > 175000 && (i_x * 250) < 250000){
    l_bath += l_displacement;
  }
  
  return l_bath;
}

