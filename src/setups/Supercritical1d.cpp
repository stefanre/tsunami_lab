/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * One-dimensional example to show Supercritical flows
 **/
#include "Supercritical1d.h"

tsunami_lab::setups::Supercritical1d::Supercritical1d( t_real i_momentum ) {

  m_momentum = i_momentum;
}

tsunami_lab::t_real tsunami_lab::setups::Supercritical1d::getHeight( t_real i_x,
                                                                   t_real      ) const {
  if( i_x > 8 && i_x < 12 ){
    return t_real(0.13) + t_real( 0.05 ) * ( i_x-10 )*(i_x-10);
  }
  else {
    return t_real(0.33);
  }
}

tsunami_lab::t_real tsunami_lab::setups::Supercritical1d::getMomentumX( t_real ,
                                                                      t_real ) const {
  return m_momentum;
}

tsunami_lab::t_real tsunami_lab::setups::Supercritical1d::getMomentumY( t_real,
                                                                      t_real ) const {
  return 0;
}

tsunami_lab::t_real tsunami_lab::setups::Supercritical1d::getBathymetry( t_real i_x,
                                                                       t_real ) const {
  if( i_x > 8 && i_x < 12 ){
    return t_real(-0.13) - t_real( 0.05 ) * ( i_x-10 )*(i_x-10);
  }
  else {
    return t_real(-0.33);
  }
}

