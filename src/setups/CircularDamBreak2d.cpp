/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
* Two-dimensional example of circular dam break
 **/
#include "CircularDamBreak2d.h"
#include <cmath>

tsunami_lab::setups::CircularDamBreak2d::CircularDamBreak2d( t_real i_heightDam, 
                                                             t_real i_heightLake ) {
  m_heightDam = i_heightDam;
  m_heightLake = i_heightLake;

}

tsunami_lab::t_real tsunami_lab::setups::CircularDamBreak2d::getHeight( t_real i_x,
                                                                        t_real i_y ) const {
  // calculate dam location 
  t_real l_x = i_x - 50 ;
  t_real l_y = i_y - 50 ;
  t_real l_damLocation = l_x * l_x + l_y * l_y;
  l_damLocation = std::sqrt( l_damLocation );

  if( l_damLocation < 10){
    return 20;
  } else if( i_x < 70 && i_x >30 && i_y < 20 && i_y >10){
    return 0;
  } else { 
    return 7;
  }
}

tsunami_lab::t_real tsunami_lab::setups::CircularDamBreak2d::getMomentumX( t_real,
                                                                           t_real) const {
  return 0;
}

tsunami_lab::t_real tsunami_lab::setups::CircularDamBreak2d::getMomentumY( t_real,
                                                                           t_real) const {
  return 0;
}

tsunami_lab::t_real tsunami_lab::setups::CircularDamBreak2d::getBathymetry( t_real i_x,
                                                                            t_real i_y) const {
  if( i_x < 70 && i_x >30 && i_y < 20 && i_y >10){
    return 15;
  }
  else {
    return -7;
  }

}