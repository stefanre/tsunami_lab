/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Setup to continue from a checkpoint.
 **/
#ifndef TSUNAMI_LAB_SETUPS_CHECKPOINT_H
#define TSUNAMI_LAB_SETUPS_CHECKPOINT_H

#include "Setup.h"

namespace tsunami_lab {
  namespace setups {
    class CheckPoint;
  }
}

class tsunami_lab::setups::CheckPoint: public Setup {

  private:

    //! number of x values
    t_idx m_x = 0;
    
    //! number of y values 
    t_idx m_y = 0;

    //! number of values of the time dimension
    t_idx m_time = 0;


    //! array for the x values
    t_real * m_XArray = nullptr;
    //! array for the y values
    t_real * m_YArray = nullptr;
    //! array for the bathymetry values
    t_real * m_bathData = nullptr;
    //! array for the h values
    t_real * m_hData = nullptr;
    //! array for the hu values
    t_real * m_huData = nullptr;
    //! array for the hv values
    t_real * m_hvData = nullptr;

    //! name of the bathymetry input file
    const char * m_inputFile = "checkpoint.nc";

    /**
      * Gets the nearest value inside the array
      *
      * @param i_array array of values ordered in ascending order
      * @param i_arraySize size ouf the input array
      * @param i_pos value for which the next array entry should be found
      **/  
    static t_real getNearestIndex( t_real * i_array,
                                   t_idx    i_arraySize,
                                   t_real   i_pos);
 
  public:

    /**
     * Constructor.
     *  
     * @param i_x number of x Cells
     * @param i_y number of y Cells;
     **/
    CheckPoint( t_idx i_x,
                t_idx i_y );

    /**
      * Destructor which frees all allocated memory.
      */
    ~CheckPoint();

    /**
     * Gets the water height at a given point.
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return height at the given point.
     **/
    t_real getHeight( t_real i_x,
                      t_real i_y ) const;

    /**
     * Gets the momentum in x-direction.
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return momentum in x-direction.
     **/
    t_real getMomentumX( t_real i_x,
                         t_real i_y ) const;

    /**
     * Gets the momentum in y-direction.
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return momentum in y-direction.
     **/
    t_real getMomentumY( t_real i_x,
                         t_real i_y ) const;

    /**
     *
     * Gets the bathymetry at a given point
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return bathymetry at the given point.
     **/
     t_real getBathymetry( t_real i_x,
                           t_real i_y ) const;

};
    



#endif
