/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * One-dimensional Rarefaction-Rarefaction-wave Problem.
 **/
#ifndef TSUNAMI_LAB_SETUPS_RARE_RARE_WAVE_1D_H
#define TSUNAMI_LAB_SETUPS_RARE_RARE_WAVE_1D_H

#include "Setup.h"

namespace tsunami_lab {
    namespace setups {
        class RareRareWave1d;
    }
}
/**
 * 1d shock-shock wave setup.
 **/
class tsunami_lab::setups::RareRareWave1d: public Setup {
    private:
        //! height on the left side 
        t_real m_heightLeft = 0;
        
        //! height on the right side
        t_real m_heightRight = 0;

        //! momentum on the left side 
        t_real m_momentumLeft = 0;
        
        //! momentum on the right side
        t_real m_momentumRight = 0;

        //! location of the dam
        t_real m_locationSmash = 0;

    public:
        /**
         * Constructor.
         *
         * @param i_heightLeft water height on the left side.
         * @param i_heightRight water height on the right side.
         * @param i_particleVelocityLeft momentum on the left side.
         * @param i_particleVelocityRight momentum on the right side.
         * @param i_locationSmash location (x-coordinate) Smash.
         **/
         RareRareWave1d( t_real i_heightLeft,
                         t_real i_heightRight,
                         t_real i_momentumLeft,
                         t_real i_momentumRight,
                         t_real i_locationSmash );

        /**
         * Gets the water height at a given point.
         *
         * @param i_x x-coordinate of the queried point.
         * @return height at the given point.
         **/
        t_real getHeight( t_real i_x,
                          t_real      ) const;

        /**
         * Gets the momentum in x-direction.
         *
         * @return momentum in x-direction.
         **/
        t_real getMomentumX( t_real i_x,
                             t_real ) const;

        /**
         * Gets the momentum in y-direction.
         *
         * @return momentum in y-direction.
         **/
         t_real getMomentumY( t_real,
                              t_real ) const;

        /**
        *
        * Gets the bathymetry at a given point
        *
        * @param i_x x-coordinate of the queried point.
        * @return bathymetry at the given point.
        **/
        t_real getBathymetry( t_real i_x,
                            t_real) const;

};

#endif