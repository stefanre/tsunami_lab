/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Tests the Rarefaction-Rarefaction wave setup.
 */
#include <catch2/catch.hpp>
#include "RareRareWave1d.h"

TEST_CASE( "Test the one-dimesional Rare-Rare wave setup.", "[RareRareWave1d]") {
    tsunami_lab::setups::RareRareWave1d l_rareRare( 33,
                                                    4,
                                                    -7,
                                                    2,
                                                    5 );
  // left side
  REQUIRE( l_rareRare.getHeight( 1, 0 ) == 33 );

  REQUIRE( l_rareRare.getMomentumX( 1, 0 ) == -7 );

  REQUIRE( l_rareRare.getMomentumY( 1, 0 ) == 0 );

  REQUIRE( l_rareRare.getHeight( 1, 5 ) == 33 );

  REQUIRE( l_rareRare.getMomentumX( 1, 5 ) == -7 );

  REQUIRE( l_rareRare.getMomentumY( 1, 2 ) == 0 );

  // right side
  REQUIRE( l_rareRare.getHeight( 9, 0 ) == 4 );

  REQUIRE( l_rareRare.getMomentumX( 9, 0 ) == 2 );

  REQUIRE( l_rareRare.getMomentumY( 9, 0 ) == 0 );

  REQUIRE( l_rareRare.getHeight(9, 5 ) == 4 );

  REQUIRE( l_rareRare.getMomentumX( 9, 5 ) == 2 );

  REQUIRE( l_rareRare.getMomentumY( 9, 2 ) == 0 );
}