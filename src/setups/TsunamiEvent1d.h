/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * One-dimensional example to show tsunami near japan.
 **/
#ifndef TSUNAMI_LAB_SETUPS_TSUNAMIEVENT_1D_H
#define TSUNAMI_LAB_SETUPS_TSUNAMIEVENT_1D_H

#include "Setup.h"
#include <vector>


namespace tsunami_lab {
  namespace setups {
    class TsunamiEvent1d;
  }
}

/**
 * TsunamiEvent1d example is just possible with the file tsunami1d_data.csv!.
 **/
class tsunami_lab::setups::TsunamiEvent1d: public Setup {
  private:

    //! momentum in the water.
    t_real m_momentum = 0;

    //! Bathymetry of the seabed, usable only for this case
    t_real m_bathymetry[1763] = {0};

    t_real m_delta = 20.0;

    unsigned int m_dataSize = 1763;

        
  public:
    /**
     * Constructor.
     *
     * @param i_momentum momentum in the water.
     **/
    TsunamiEvent1d( t_real i_momentum );


    /**
     * Gets the water height at a given point.
     *
     * @param i_x x-coordinate of the queried point.
     * @return height at the given point.
     **/
    t_real getHeight( t_real i_x,
                      t_real      ) const;

    /**
     * Gets the momentum in x-direction.
     *
     * @return momentum in x-direction.
     **/
    t_real getMomentumX( t_real ,
                         t_real ) const;

    /**
     * Gets the momentum in y-direction.
     *
     * @return momentum in y-direction.
     **/
    t_real getMomentumY( t_real,
                         t_real ) const;

    /**
     *
     * Gets the bathymetry at a given point
     *
     * @param i_x x-coordinate of the queried point.
     * @return bathymetry at the given point.
     **/
     t_real getBathymetry( t_real i_x,
                           t_real) const;

};

#endif