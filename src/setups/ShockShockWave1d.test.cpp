/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Tests the shock-shock wave setup.
 */
#include <catch2/catch.hpp>
#include "ShockShockWave1d.h"

TEST_CASE( "Test the one-dimesional Shock-shock setup.", "[ShockShockWave1d]") {
    tsunami_lab::setups::ShockShockWave1d l_shockShock( 12,
                                                        15,
                                                        4,
                                                        3 );
  // left side
  REQUIRE( l_shockShock.getHeight( 2, 0 ) == 12 );

  REQUIRE( l_shockShock.getMomentumX( 2, 0 ) == 4 );

  REQUIRE( l_shockShock.getMomentumY( 2, 0 ) == 0 );

  REQUIRE( l_shockShock.getHeight( 2, 5 ) == 12 );

  REQUIRE( l_shockShock.getMomentumX( 2, 5 ) == 4 );

  REQUIRE( l_shockShock.getMomentumY( 2, 2 ) == 0 );

  // right side
  REQUIRE( l_shockShock.getHeight( 4, 0 ) == 15 );

  REQUIRE( l_shockShock.getMomentumX( 4, 0 ) == -4 );

  REQUIRE( l_shockShock.getMomentumY( 4, 0 ) == 0 );

  REQUIRE( l_shockShock.getHeight( 4, 5 ) == 15 );

  REQUIRE( l_shockShock.getMomentumX( 4, 5 ) == -4 );

  REQUIRE( l_shockShock.getMomentumY( 4, 2 ) == 0 );
}