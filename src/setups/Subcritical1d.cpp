/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * One-dimensional example to show subcritical flows
 **/
#include "Subcritical1d.h"

tsunami_lab::setups::Subcritical1d::Subcritical1d( t_real i_momentum ) {

  m_momentum = i_momentum;
}

tsunami_lab::t_real tsunami_lab::setups::Subcritical1d::getHeight( t_real i_x,
                                                                   t_real      ) const {
  if( i_x > 8 && i_x < 12 ){
    return t_real(1.8) + t_real( 0.05 ) * ( i_x-10 )*(i_x-10);
  }
  else {
    return t_real(2);
  }
}

tsunami_lab::t_real tsunami_lab::setups::Subcritical1d::getMomentumX( t_real ,
                                                                      t_real ) const {
  return m_momentum;
}

tsunami_lab::t_real tsunami_lab::setups::Subcritical1d::getMomentumY( t_real,
                                                                      t_real ) const {
  return 0;
}

tsunami_lab::t_real tsunami_lab::setups::Subcritical1d::getBathymetry( t_real i_x,
                                                                       t_real ) const {
  if( i_x > 8 && i_x < 12 ){
    return t_real(-1.8) - t_real( 0.05 ) * ( i_x-10 )*(i_x-10);
  }
  else {
    return t_real(-2);
  }
}

