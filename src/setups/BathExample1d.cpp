/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * One-dimensional example to show effect of bathymetry
 **/
#include "BathExample1d.h"

tsunami_lab::setups::BathExample1d::BathExample1d( t_real i_momentumLeft,
                                                   t_real i_momentumRight,
                                                   t_real i_locationDam ) {

  m_momentumLeft = i_momentumLeft;
  m_momentumRight = i_momentumRight;
  m_locationDam = i_locationDam;
}

tsunami_lab::t_real tsunami_lab::setups::BathExample1d::getHeight( t_real i_x,
                                                                   t_real      ) const {
  if( i_x > 5  ){
    return 4;
  }
  else {
    return 8;
  }
}

tsunami_lab::t_real tsunami_lab::setups::BathExample1d::getMomentumX( t_real i_x,
                                                                      t_real ) const {
  if( i_x < m_locationDam ) {
    return m_momentumLeft;
  }
  else {
    return m_momentumRight;
  }
}

tsunami_lab::t_real tsunami_lab::setups::BathExample1d::getMomentumY( t_real,
                                                                      t_real ) const {
  return 0;
}

tsunami_lab::t_real tsunami_lab::setups::BathExample1d::getBathymetry( t_real i_x,
                                                                       t_real ) const {
  if( i_x > 5 ){
    return -4;
  }
  else {
    return -8;
  }

}