/**
 * @author Philipp Vaupel, Stefan Remke
 *
 * @section DESCRIPTION
 * Two-dimensional TohokuEvent2d from 2011 setup with netcdf data
 **/
#ifndef TSUNAMI_LAB_SETUPS_TOHOKU_EVENT_2D_H
#define TSUNAMI_LAB_SETUPS_TOHOKU_EVENT_2D_H

#include "Setup.h"

namespace tsunami_lab {
  namespace setups {
    class TohokuEvent2d;
  }
}

/**
 * Two-dimensional TohokuEvent2d from 2011 setup with netcdf data
 **/
class tsunami_lab::setups::TohokuEvent2d: public Setup {

  private:

    /**
      * Gets the nearest value inside the array
      *
      * @param i_array array of values ordered in ascending order
      * @param i_arraySize size ouf the input array
      * @param i_pos value for which the next array entry should be found
      **/  
    static t_real getNearestIndex( t_real * i_array,
                                   t_idx    i_arraySize,
                                   t_real   i_pos);
 
  public:

    //! number of x values for the bathymetry file
    t_idx m_xB = 10800;
    
    //! number of y values for the bathymetry file
    t_idx m_yB = 6000;

    //! number of x values for the displacement file.
    t_idx m_xD = 10801;

    //! number of y values for the displacement file.
    t_idx m_yD = 6001;

    t_real * m_bathX = nullptr;
    
    t_real * m_bathY = nullptr;

    t_real * m_bathData = nullptr;

    t_real * m_dispX = nullptr;

    t_real * m_dispY = nullptr;

    t_real * m_dispData = nullptr;

    //! name of the bathymetry input file
    const char * m_inputFileB = "inputData/tohoku_gebco20_usgs_250m_bath.nc";

    //! name of the displacements input file
    const char * m_inputFileD = "inputData/tohoku_gebco20_usgs_250m_displ.nc";


    /**
     * Constructor.
     *
     **/
    TohokuEvent2d();

    /**
      * Destructor which frees all allocated memory.
      */
    ~TohokuEvent2d();

    /**
     * Gets the water height at a given point.
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return height at the given point.
     **/
    t_real getHeight( t_real i_x,
                      t_real i_y ) const;

    /**
     * Gets the momentum in x-direction.
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return momentum in x-direction.
     **/
    t_real getMomentumX( t_real i_x,
                         t_real i_y ) const;

    /**
     * Gets the momentum in y-direction.
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return momentum in y-direction.
     **/
    t_real getMomentumY( t_real i_x,
                         t_real i_y ) const;

    /**
     *
     * Gets the bathymetry at a given point
     *
     * @param i_x x-coordinate of the queried point.
     * @param i_y y-coordinate of the queried point.
     * @return bathymetry at the given point.
     **/
     t_real getBathymetry( t_real i_x,
                           t_real i_y ) const;

};

#endif