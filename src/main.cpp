/**
 * @author Alexander Breuer (alex.breuer AT uni-jena.de)
 *
 * @section DESCRIPTION
 * Entry-point for simulations.
 **/
#include "patches/WavePropagation1d.h"
#include "patches/WavePropagation2d.h"
#include "setups/DamBreak1d.h"
#include "setups/ShockShockWave1d.h"
#include "setups/RareRareWave1d.h"
#include "setups/BathExample1d.h"
#include "setups/Subcritical1d.h"
#include "setups/Supercritical1d.h"
#include "setups/TsunamiEvent1d.h"
#include "setups/CircularDamBreak2d.h"
#include "setups/ArtificialTsunami2d.h"
#include "setups/TsunamiEvent2d.h"
#include "setups/ChileEvent2d.h"
#include "setups/TohokuEvent2d.h"
#include "setups/CheckPoint.h"
#include "io/Csv.h"
#include "io/Stations.h"
#include "pugixml/src/pugixml.hpp"
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <fstream>
#include <limits>
#include <string>
#include <netcdf.h>
#include <stdlib.h> 
#include <time.h> 
#include "io/NetCdf.h"
#include <chrono>
#include <omp.h>


int main() {
  
  pugi::xml_document l_setupFile;
  if (!l_setupFile.load_file("./configuration.xml")) {
    std::cerr << "Fehler beim Einladen der Konfigurationsdatei (.xml)..." << std::endl;
    return EXIT_FAILURE;
  }
  
  // pugi node with settings
  pugi::xml_node l_settings = l_setupFile.child("configuration").child("settings");
  
  // time measurement
  std::chrono::steady_clock::time_point l_tp0, l_tp1;
  std::chrono::duration< double > l_dur;
  
  // number of cells in x- and y-direction
  tsunami_lab::t_idx l_nx;
  tsunami_lab::t_idx l_ny;

  // set cell size
  tsunami_lab::t_real l_dx = 0;
  tsunami_lab::t_real l_dy = 0;

  // set length and width of the domain
  tsunami_lab::t_real l_lw = 0;

  // set x length of domain
  tsunami_lab::t_real l_domainSizeX = 0;
  tsunami_lab::t_real l_domainSizeY = 0;
 
  // set coice for Solver Type
  unsigned short l_solv = 0;

  // set choice for Setup
  unsigned short l_set = 0;

  // set choice for Boundary conditions
  unsigned short l_boundary = 0;

  // set stations frequency
  tsunami_lab::t_real l_freq = 100.0;


  time_t l_runtime = time(NULL);


  // set offset of the domain
  tsunami_lab::t_real l_offsetX = 0;
  tsunami_lab::t_real l_offsetY = 0;

  // set choice for output data coarse factor
  tsunami_lab::t_idx l_k;

  std::cout << "####################################" << std::endl;
  std::cout << "### Tsunami Lab                  ###" << std::endl;
  std::cout << "###                              ###" << std::endl;
  std::cout << "### https://scalable.uni-jena.de ###" << std::endl;
  std::cout << "####################################" << std::endl;
  std::cout << "start time:  " << ctime(&l_runtime) << std::endl; 

  
  l_lw = l_settings.attribute("domainSize").as_double();
  if ( l_lw < 1) {
    std::cerr << "invalid domain size selected" << std::endl;
    return EXIT_FAILURE;
  }
  l_domainSizeX = l_settings.attribute("domainSizeX").as_double();
  if ( l_domainSizeX < 1) {
    std::cerr << "invalid domain size selected (x)" << std::endl;
    return EXIT_FAILURE;
  }
  l_domainSizeY = l_settings.attribute("domainSizeY").as_double();
  if ( l_domainSizeX < 1) {
    std::cerr << "invalid domain size selected (y)" << std::endl;
    return EXIT_FAILURE;
  }

  l_nx = l_settings.attribute("cellAmountX").as_int();
  if( l_nx < 1 ) {
    std::cerr << "invalid number of cells in x direction" << std::endl;
    return EXIT_FAILURE;
  }
  l_ny = l_settings.attribute("cellAmountY").as_int();
  if( l_ny < 1 ) {
    std::cerr << "invalid number of cells in y direction" << std::endl;
    return EXIT_FAILURE;
  }
  l_dx = l_lw / l_nx;
  l_dy = l_lw / l_ny;
    
  l_solv = l_settings.attribute("solver").as_int();
  if( l_solv < 1 || l_solv > 2 ) {
    std::cerr << "invalid Solver Type" << std::endl;
    return EXIT_FAILURE;
  }

  l_set = l_settings.attribute("setup").as_int();
  if( l_set < 1 || l_set > 12) {
    std::cerr << "invalid Setup Type" << std::endl;
    return EXIT_FAILURE;
  }

  l_boundary = l_settings.attribute("boundaryCondition").as_int();
  if( l_boundary < 1 || l_boundary > 5) {
    std::cerr << "invalid Boundary Type" << std::endl;
    return EXIT_FAILURE;
  }

  l_freq = l_settings.attribute("stationsFrequency").as_float();
  if ( l_freq <= 0 ) {
    std::cerr << "invalid stations writing frequency selected" << std::endl;
    return EXIT_FAILURE;
  }
  l_k = l_settings.attribute("dataCoarseFactor").as_int();
  if( l_k < 1 ) {
    std::cout << "warning: inavlid data coarse factor reset to 1!";
    l_k = 1;
  }
  l_offsetX = l_settings.attribute("offsetX").as_float();
  l_offsetY = l_settings.attribute("offsetY").as_float();

  std::ifstream l_fileCheckpoint("checkpoint.nc");
  // construct setup
  tsunami_lab::setups::Setup *l_setup;

  // check if checkpoint exists
  if(l_fileCheckpoint.good()){
    l_set = 13;
  } 
  if( l_set == 1 ) {
    l_setup = new tsunami_lab::setups::DamBreak1d( 14,
                                                  3.5,
                                                  0,
                                                  0.7,
                                                  5 );
  } else if( l_set == 2) {
    l_setup = new tsunami_lab::setups::ShockShockWave1d( 4,
                                                         4,
                                                         3,
                                                         5 );
  } else if( l_set == 3){
    l_setup = new tsunami_lab::setups::RareRareWave1d( 4,
                                                       4,
                                                       -3,
                                                       3,
                                                       5 );
  } else if( l_set == 4){
    l_setup = new tsunami_lab::setups::BathExample1d( 20,
                                                      0,
                                                      3);
  } else if( l_set == 5){ 
    // before using it set width to 25 !!
    // for this case you adjust the boundary conditions!
    l_dx = 25.0 / l_nx;
    l_setup = new tsunami_lab::setups::Subcritical1d( 4.42 );
  }else if( l_set == 6){ 
    // before using it set width to 25 !!
    // for this case you adjust the boundary conditions!
    l_dx = 25.0 / l_nx;
    l_setup = new tsunami_lab::setups::Supercritical1d( 0.18 );
  } else if( l_set == 7){
    // make sure that width is setup correctly
    l_solv = 2;
    l_nx = 1763;
    l_dx = 1763.0 / l_nx;
    std::cout << "*******************" << std::endl;
    l_setup = new tsunami_lab::setups::TsunamiEvent1d(0.0);
  } else if (l_set == 8){
    l_ny = l_nx;
    l_setup = new tsunami_lab::setups::CircularDamBreak2d( 10, 5);
  } else if( l_set == 9){
    l_solv = 2 ;
    l_lw = 10000;
    l_ny = l_nx;
    l_dx = l_lw / l_nx;
    l_dy = l_lw / l_ny;
    l_setup = new tsunami_lab::setups::ArtificialTsunami2d();
  } else if( l_set == 10){
    l_solv = 2;
    l_lw = 10000;
    l_ny = l_nx;
    l_dx = l_lw / l_nx;
    l_dy = l_lw / l_ny;
    l_setup = new tsunami_lab::setups::TsunamiEvent2d();
  } else if( l_set == 11){
    l_dx = l_domainSizeX / l_nx;
    l_dy = l_domainSizeY / l_ny;
    l_setup = new tsunami_lab::setups::ChileEvent2d();
  } else if( l_set == 12 ){
    l_dx = l_domainSizeX / l_nx;
    l_dy = l_domainSizeY / l_ny;
    l_setup = new tsunami_lab::setups::TohokuEvent2d();
  } else if( l_set == 13 ){
    l_dx = l_domainSizeX / l_nx;
    l_dy = l_domainSizeY / l_ny;
    l_setup = new tsunami_lab::setups::CheckPoint( l_nx, l_ny );
  }

  std::cout << "runtime configuration" << std::endl;
  std::cout << "  number of cells in x-direction: " << l_nx << std::endl;
  std::cout << "  number of cells in y-direction: " << l_ny << std::endl;
  std::cout << "  X cell size:                    " << l_dx << std::endl;
  std::cout << "  Y cell size:                    " << l_dy << std::endl;
  std::cout << "  stations writing frequency:     " << l_freq << std::endl;


  // maximum observed height in the setup
  tsunami_lab::t_real l_hMax = std::numeric_limits< tsunami_lab::t_real >::lowest();

  // construct solver
  tsunami_lab::patches::WavePropagation *l_waveProp;
  if( l_set > 7){
    l_waveProp = new tsunami_lab::patches::WavePropagation2d( l_nx, l_ny );
  } else {
    l_waveProp = new tsunami_lab::patches::WavePropagation1d( l_nx );
  }

  //setup stations
  pugi::xml_node l_stationsAllNodes = l_setupFile.child("configuration").child("stations");
  std::vector<tsunami_lab::io::Stations*> stationsVector;
  for (pugi::xml_node l_stationNode : l_stationsAllNodes) {
    stationsVector.push_back(new tsunami_lab::io::Stations( l_stationNode.attribute("Name").as_string(),
                                                            l_stationNode.attribute("X").as_float(),
                                                            l_stationNode.attribute("Y").as_float(),
                                                            l_dx,
                                                            l_freq,
                                                            l_nx ));
  }
  
  // help variable to implement frequency
  tsunami_lab::t_real l_frequency = 0;


  // set up solver
  #pragma omp parallel for
  for( tsunami_lab::t_idx l_cy = 0; l_cy < l_ny; l_cy++ ) {
    tsunami_lab::t_real l_y = l_cy * l_dy; 
    if( l_set > 9){
      l_y = l_cy * l_dy + l_offsetY;
    }

    for( tsunami_lab::t_idx l_cx = 0; l_cx < l_nx; l_cx++ ) {
      tsunami_lab::t_real l_x = l_cx * l_dx; 
      if( l_set > 9){
        l_x = l_cx * l_dx + l_offsetX;
      }

      // get initial values of the setup
      tsunami_lab::t_real l_h = l_setup->getHeight( l_x,
                                                    l_y );
      l_hMax = std::max( l_h, l_hMax );

      tsunami_lab::t_real l_hu = l_setup->getMomentumX( l_x,
                                                        l_y );
      tsunami_lab::t_real l_hv = l_setup->getMomentumY( l_x,
                                                        l_y );

      tsunami_lab::t_real l_b = l_setup->getBathymetry( l_x,
                                                        l_y );

      // set initial values in wave propagation solver
      l_waveProp->setHeight( l_cx,
                             l_cy,
                             l_h );

      l_waveProp->setMomentumX( l_cx,
                                l_cy,
                                l_hu );

      l_waveProp->setMomentumY( l_cx,
                                l_cy,
                                l_hv );

      l_waveProp->setBathymetry( l_cx,
                                 l_cy,
                                 l_b );

    }
  }

  // derive maximum wave speed in setup; the momentum is ignored
  tsunami_lab::t_real l_speedMax = std::sqrt( 9.81 * l_hMax );

  // TODO: implement diffrent cell sizes in x and y direction
  // derive constant time step; changes at simulation time are ignored
  tsunami_lab::t_real l_dt = std::max( (0.45 * l_dx / l_speedMax), (0.45 * l_dy / l_speedMax));
  
  // derive scaling for a time step
  // TODO: Potentially different cell size y
  tsunami_lab::t_real l_scaling = l_dt / l_dx;

  // set up time and print control
  tsunami_lab::t_idx  l_timeStep = 0;
  tsunami_lab::t_idx  l_nOut = 0;
  tsunami_lab::t_idx  l_checkpointCount = 0;
  tsunami_lab::t_real l_endTime = 7200.0;
  tsunami_lab::t_real l_simTime = 0;
  
  std::cout << "init Netcdf file (output.nc)" << std::endl;
  tsunami_lab::io::NetCdf * l_netcdf;
  tsunami_lab::io::NetCdf * l_checkpoint;
  l_netcdf = new tsunami_lab::io::NetCdf( l_nx, l_ny, "output.nc", 1, l_k );
  l_checkpoint = new tsunami_lab::io::NetCdf( l_nx, l_ny, "checkpoint.nc", 0, 1 );

  // init netcdf files
  l_netcdf->initialize( l_dx, 
                        l_dy, 
                        l_waveProp->getStride(), 
                        l_waveProp->getBathymetry(), 
                        l_offsetX, 
                        l_offsetY, 
                        "output.nc" );

  l_checkpoint->initialize( l_dx, 
                            l_dy, 
                            l_waveProp->getStride(), 
                            l_waveProp->getBathymetry(), 
                            l_offsetX, 
                            l_offsetY, 
                            "checkpoint.nc" );
  

  std::cout << "entering time loop" << std::endl;

  l_tp0 = std::chrono::steady_clock::now();
  // iterate over time
  while( l_simTime < l_endTime ){
    if( l_timeStep % 25 == 0 ) {
      std::cout << "  simulation time / #time steps: "
                << l_simTime << " / " << l_timeStep << std::endl;

      if( l_set < 8){
        std::string l_path = "solution_" + std::to_string(l_nOut) + ".csv";
        std::cout << "  writing wave field to " << l_path << std::endl;
        std::ofstream l_file;
        l_file.open( l_path  );

        tsunami_lab::io::Csv::write(l_dx,
                                    l_nx,
                                    l_ny,
                                    l_waveProp->getStride(),
                                    l_waveProp->getHeight(),
                                    l_waveProp->getBathymetry(),
                                    l_waveProp->getMomentumX(),
                                    l_waveProp->getMomentumY(),
                                    l_file );
        l_file.close();
      }
      if(l_set > 7 ){
        // writing to netcdf file (output.nc)
        l_netcdf->write( l_waveProp->getHeight(), 
                         l_waveProp->getMomentumX(), 
                         l_waveProp->getMomentumY(), 
                         l_waveProp->getStride() , 
                         l_nOut, 
                         l_simTime,
                         "output.nc",
                         1 ,
                         0);
        std::cout << "netCDF written time step: " << l_nOut << std::endl;
        
        // writing checkpoint 
        if(l_nOut % 10 == 0){
          l_checkpoint->write( l_waveProp->getHeight(), 
                              l_waveProp->getMomentumX(), 
                              l_waveProp->getMomentumY(), 
                              l_waveProp->getStride() , 
                              l_nOut, 
                              l_simTime,
                              "checkpoint.nc",
                              0,
                              l_checkpointCount );
          l_checkpointCount++;
          std::cout << "**Checkpoint**" << std::endl; 
        }
      }
      l_nOut++;
    }

    if( l_set > 7){
      l_waveProp->setGhostOutflow();
    } else {
      if( l_boundary == 1 ){
        l_waveProp->setGhostOutflow();
      } else if( l_boundary == 2 ){
        l_waveProp->setGhostOutflowReflecting();
      } else if( l_boundary == 3 ){
        l_waveProp->setGhostReflectingOutflow();
      } else {
        l_waveProp->setGhostReflecting();;
      }
    }

    l_waveProp->timeStep( l_scaling,
                          l_solv );
                    
    // if( l_frequency == 0 || l_frequency > l_freq ){
    //     for (unsigned short i = 0; i < stationsVector.size(); i++) {
    //       stationsVector[i]->write( l_simTime,
    //                                 l_waveProp->getHeight(),
    //                                 l_waveProp->getMomentumX(),
    //                                 l_waveProp->getMomentumY());
    //       l_frequency = 0;
    //     }
    // }

    l_timeStep++;
    l_simTime += l_dt;
    l_frequency += l_dt;
  }
  l_tp1 = std::chrono::steady_clock::now();

  std::cout << "finished time loop" << std::endl;
  std::cout << "timesteps made: " << l_timeStep-1 << std::endl;
  
  tsunami_lab::t_idx l_nCellUpdates = (l_nx+2) * (l_ny+1) + (l_nx+1) * (l_ny+2);
  std::cout << "l_nCellUpdates:  " << l_nCellUpdates << std::endl;
 
  // free memory
  std::cout << "freeing memory" << std::endl;
  for (unsigned short i = 0; i < stationsVector.size(); i++) {
    stationsVector[i]->~Stations();
  }
  delete l_setup;
  delete l_waveProp;
  stationsVector.clear();
  // delete l_netcdf;
  // delete l_checkpoint;

  l_runtime = time(NULL);  
  std::cout << " end time:  " << ctime(&l_runtime) << std::endl;

  l_dur = std::chrono::duration_cast< std::chrono::duration< double> >( l_tp1 - l_tp0 );
  std::cout << "  duration: " << l_dur.count() << " seconds" << std::endl;

  tsunami_lab::t_real l_timePerUpdate = l_dur.count() / (l_timeStep-1);
  std::cout << "  time per full domain update:  " << l_timePerUpdate << std::endl;

  tsunami_lab::t_real l_timePerCellUpdate = l_timePerUpdate / l_nCellUpdates;
  std::cout << "  time per cell update:  " << l_timePerCellUpdate << std::endl;

  std::cout << "  cell updates per second:  " << 1 / l_timePerCellUpdate << std::endl;

  std::cout << "finished, exiting" << std::endl;
  return EXIT_SUCCESS;
}
