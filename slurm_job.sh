#!/bin/bash
#SBATCH --job-name=tsunami01
#SBATCH --output=tsunami.out
#SBATCH --error=tsunami.err
#SBATCH --partition=s_hadoop
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=72
#SBATCH --time=05:00:00


scons

OMP_NUM_THREADS=18 OMP_PLACES={0:18} ./build/tsunami_lab


